CREATE TABLE public.participante(
	uid SERIAL NOT NULL PRIMARY KEY,
	login VARCHAR(20) NOT NULL,
	senha VARCHAR(50) NOT NULL,
	nome_completo VARCHAR(100) NOT NULL,
	cpf VARCHAR(11) NOT NULL,
	rg VARCHAR(15) NOT NULL,
	email VARCHAR(50),
	nome_cracha VARCHAR(20),
	logradouro VARCHAR(100),
	complemento VARCHAR(50),
	bairro VARCHAR(20),
	cep VARCHAR(100), 
	cidade VARCHAR(50),
	estado VARCHAR(20),
	tipo_telefone VARCHAR(20), 
	ddd_telefone_telefone VARCHAR(20),
	tel_telefone VARCHAR(20),
	data_nasc VARCHAR(20),
	estado_civil VARCHAR(20),
	escolaridade VARCHAR(20),
	profissao VARCHAR(20),
	inst_orig VARCHAR(50),
	como_fic_saben VARCHAR(200),
	outra_pessoa VARCHAR(50),
	foto bytea,
	tipo VARCHAR(20),
	sexo VARCHAR(20),
	image_id INTEGER
);


CREATE TABLE public.eventos
(
  uid SERIAL NOT NULL PRIMARY KEY,
  titulo character varying(50),
  descricao character varying(200),
  informacoes_imp character varying(200),
  preco character varying(10),
  periodo VARCHAR(20),
  forma_pagamento VARCHAR(30),
  id_local INTEGER,
  id_entidade INTEGER
);

CREATE TABLE inscricoes(
	id_participante INTEGER,
	id_evento INTEGER,
	data DATE,
	pago INTEGER,
	PRIMARY KEY(id_participante,id_evento)

);

CREATE TABLE public.local(
	id_local SERIAL PRIMARY KEY,
	nome_local VARCHAR(50),
	endereco_local VARCHAR(100),
	coord_geo VARCHAR(100),
	tipo_telefone VARCHAR(30),
	tel_telefone VARCHAR(30),
	ddd_telefone VARCHAR(10)
);
 
CREATE TABLE public.entidade(
	id_entidade SERIAL PRIMARY KEY,
	nome_entidade VARCHAR(50),
	descricao_entidade VARCHAR(200)
);