<%@include file="/view/include/loginCheck.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Página Inicial</title>
    </head>
    <body class="w3-light-grey">
        
        <div class="w3-content" style="max-width:1200px">
            <h1>Bem-vindo,<a> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/update?id=${participanteLogado.uid}" >
                                   <c:out value="${participanteLogado.nome_completo}"/>
                                </a>!</h1>
            
            
           <img src="${pageContext.servletContext.contextPath}/participante/getImage.action?id=${participanteLogado.uid}"    height="200" width="200">
           <hr>      
            <c:choose>
                <c:when test="${participanteLogado.tipo=='admin'}">
                <p>
                    <button><a href="${pageContext.servletContext.contextPath}/participante/create">Cadastro de Usuários</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento/create">Cadastro de Eventos</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/entidade/create">Cadastro de Entidade</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/local/create">Cadastro de Local</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento">Listar Eventos</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/participante">Listar Participantes e Membros</a></button>
                    
                </c:when>
                <c:when test="${participanteLogado.tipo=='participante'}">
                   <button> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/inscricoes?id=${participanteLogado.uid}" >
                                    Listar Inscricoes
                    </a></button>
                                    
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento">Listar Eventos</a></button>
                </c:when>
                <c:when test="${participanteLogado.tipo=='membro'}">
                    <p>
                    <button><a href="${pageContext.servletContext.contextPath}/participante/create">Cadastro de Usuários</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento/create">Cadastro de Eventos</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/entidade/create">Cadastro de Entidade</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/local/create">Cadastro de Local</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento">Listar Eventos</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/evento/busca">Buscar Evento</a></button>
                    <br><br><button><a href="${pageContext.servletContext.contextPath}/participante">Listar Participantes e Membros</a></button>
                    
                </c:when>
            </c:choose>
                
                               
               <br><br><button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
            
            </p>
        </div>
               
    </body>
</html>
