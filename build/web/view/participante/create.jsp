<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <script type="text/javascript" >
    
    function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('logradouro').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('uf').value=("");
            
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('logradouro').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
            document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
        
    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('logradouro').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('uf').value="...";
                
                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'http://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };
</script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
       <title>Cadastro de Participante</title>
    </head>
    <body class="w3-light-grey">
        <div class="w3-content" style="max-width:1200px">
            
            <h2 class="text-center">Novo Cadastro de usuário</h2>

            <c:choose>
                <c:when test="${param.acao == 'c'}">
                <form class="form-group" name="formParticipante" method="POST" enctype="multipart/form-data" action="${pageContext.servletContext.contextPath}/participante/create">                    
                </c:when>
                <c:otherwise>
                <form class="form-group" name="formParticipante" method="POST" action="${pageContext.servletContext.contextPath}/participante/update">                    
                    <input type="hidden" name="id" value="${u.uid}">
                </c:otherwise>    

            </c:choose>
            
                    <label class="h4">Login</label>
                    <input class="form-control" type="text" name="login" value="${u.login}" required autofocus>

                    <label class="h4">Nome Completo</label>
                    <input class="form-control" type="text" style="text-align:left;" name="nome_completo" value="${u.nome_completo}" required autofocus>
                    
                    <label class="h4">CPF</label>
                    <input class="form-control" type="text" style="text-align:left;" name="cpf" value="${u.cpf}" >

                    <label class="h4"> Foto </label>
                    <input type="file" name="foto" size="50"/>
                    <hr> 
                    <label class="h4">RG</label>
                    <input class="form-control" type="text" style="text-align:left;" name="rg" value="${u.rg}" >
                   
                    
                    <label class ="h4">Sexo</label>
                    
                    <select class="form-control"  name="sexo">
                      <option value = "masculino">Masculino</option>
                      <option value = "feminino">Feminino</option>
                      <option value = "outro">Outro</option>
                    </select>
                      
                    <label class="h4">Email</label>
                    <input class="form-control" type="email" style="text-align:left;" name="email" value="${u.email}" required autofocus>

                    <label class="h4">Nome Crachá</label>
                    <input class="form-control" type="text" style="text-align:left;" name="nome_cracha" value="${u.nome_cracha}" required autofocus>
                    <hr>              
                    <label class="h4">CEP</label>
                    <input class="form-control" type="text" style="text-align:left;" id="cep"  name="cep" value="${u.cep}" size="10" maxlength="9" onblur="pesquisacep(this.value);" />
                    
                    <label class="h4">Logradouro</label>
                    <input class="form-control" type="text" style="text-align:left;" id="logradouro" size="60" name ="logradouro" value ="${u.logradouro}" />

                    <label class="h4">Numero</label>
                    <input class="form-control" type="text" style="text-align:left;" name="complemento" value="${u.complemento}">
                    <hr>
                    <label class="h4">Bairro</label>
                    <input class="form-control" type="text" style="text-align:left;" id="bairro" name="bairro" size="40" value ="${u.bairro}" />
        
                    <label class="h4">Cidade</label>
                    <input class="form-control" type="text" style="text-align:left;" id="cidade" name="cidade" size="40" value ="${u.cidade}" />
        
                    <label class="h4">Estado</label>
                    <input class="form-control" type="text" style="text-align:left;" id="uf" name="estado" size="2" value = "${u.estado}"/>
                    
                   
                    <hr> 
                    <label class="h4">Tipo de Telefone</label>
                    <select class="form-control"  name="tipo_telefone">
                      <option value = "residencial">Residencial</option>
                      <option value = "comercial">Comercial</option>
                      <option value = "celular">Celular</option>
                      <option value = "recado">Recado</option>
                    </select>
                
                    
                    <label class="h4">DDD telefone</label>
                    <input class="form-control" type="text" style="text-align:left;" name="ddd_telefone" value="${u.telefone.ddd}" required autofocus>

                    <label class="h4">Telefone</label>
                    <input class="form-control" type="text" style="text-align:left;" name="tel_telefone" value="${u.telefone.tel_telefone}" required autofocus>
                    
                    <label class="h4">Data Nascimento</label>
                    <input class="form-control" type="date" style="text-align:left;" name="data_nasc" value="${u.data_nasc}" required autofocus>

                    <hr>
                    <label class="h4">Estado Civil</label>
                    
                   
                    <select class="form-control"  name="estado_civil">
                      <option value = "solteiro">Solteiro</option>
                      <option value = "casado">Casado</option>
                      <option value = "outro">Outro</option>
                    </select>
                        
                    <label class="h4">Escolaridade</label>
                    <select class="form-control"  name="escolaridade">
                      <option value = "ensino fundamental">Ensino Fundamental</option>
                      <option value = "ensino medio">Ensino Medio</option>
                      <option value = "ensino superior">Ensino Superior</option>
                    </select>
                    
                    <label class="h4">Profissão</label>
                    <input class="form-control" type="text" style="text-align:left;" name="profissao" value="${u.profissao}" required autofocus>
                    
                    <label class="h4">Instituição de Origem</label>
                    <input class="form-control" type="text" style="text-align:left;" name="inst_orig" value="${u.inst_orig}">
                    <hr>
                    <label class="h4">Como ficou sabendo</label>
                    <textarea class="form-control" style="text-align:left;" name="como_fic_saben"></textarea>
                    <hr>
                    <label class="h4">Outra pessoa</label>
                    <input class="form-control" type="text" style="text-align:left;" name="outra_pessoa" value="${u.outra_pessoa}" >
                    
                    <c:choose>
                    <c:when test="${participanteLogado.tipo=='admin'}">
                    <hr>
                    <label class="h4">Tipo:</label>

                    <select class="form-control"  name="tipo">
                      <option value = "admin">Administrador</option>
                      <option value = "membro">Membro</option>
                      <option value = "participante">Participante</option>
                    </select>

                    </c:when>
                    <c:otherwise>
                        <hr>
                        <label class="h4">Tipo:</label>
                      <select class="form-control"  name="tipo">
                      <option value = "participante">Participante</option>
                    </select>
                    </c:otherwise>
                    </c:choose>
                     
                    <c:choose>
                    <c:when test="${param.acao == 'c'}">
                        <label class="h4">Senha</label>
                        <input class="form-control" type="password" name="senha" required>
                        <button class="btn btn-lg btn-primary" type="submit">Cadastrar</button>
                        <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
                        <hr>
                        
                     </c:when>
                     <c:otherwise>
                     <label class="h4">Senha</label>
                     <input class="form-control" type="password" name="senha">          
                     <button class="btn btn-lg btn-primary" type="submit">Atualizar</button>  
                     <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
                     <hr>
                     
                    </c:otherwise>
           
                     </c:choose>
                        
            </form>
        </div>
    </body>
</html>
