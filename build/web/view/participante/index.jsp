<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="java.util.List"%>
<%@page import="model.Participante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Participantes</title>
    </head>
    
    <body class="w3-light-grey">
           <div class="w3-content" style="max-width:1200px">
               <h1>Participantes</h1>
            
            <table class = "w3-table-all">
                <thead>
                    <th>Login</th>
                    <th>Nome</th>
                    <th>Privilégio</th>
                    <th>Ações</th>
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${participanteList}">
                    <tr>
                        <td><c:out value="${u.login}"/></td>
                        
                <br>
                <td><c:out value="${u.nome_completo}"/></td>
                <td><c:out value="${u.tipo}"/></td>
                <td>
                    <button><a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/update?id=${u.uid}" >
                                    Editar
                                </a></button>
                               <button> <a class="btn btn-default link_excluir_usuario" href="${pageContext.servletContext.contextPath}/participante/delete?id=${u.uid}">
                                    Excluir
                                </a>  </button> 
                                    
                                   
                               <button> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/inscricoes?id=${u.uid}" >
                                    Listar Inscricoes
                                </a></button>

                        </td>
                                        
                    </tr>                    
                </c:forEach>
                    
                </tbody>
            </table>
            <br><br>
            <hr>
            <button><a href="${pageContext.servletContext.contextPath}/participante/create">
                Cadastrar novo usuário
            </a></button>    
            <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>

               
            
        </div>
            

    </body>
</html>
