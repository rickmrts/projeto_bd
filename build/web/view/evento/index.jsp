<%@include file="/view/include/loginCheck.jsp"%>
<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page import="model.Participante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Eventos</title>
    </head>
    <body class="w3-light-grey">
        
        <c:choose>
        <c:when test="${param.acao == 'n'}">
            
            <div class="w3-content" style="max-width:1200px">
            <h2> Eventos Atuais</h2>
            <table  class="w3-table-all">
                <thead>
                    <th>Título</th>
                    <th>Entidade Promotora</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Valor</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                        
                        <td><c:out value="${u.entidade.nome_entidade}"/></td>
                        <td><c:out value="${u.dt_inicio}"/></td>
                        <td><c:out value="${u.dt_final}"/></td>
                        <td>R$ <c:out value="${u.preco}"/></td>
                         <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td><c:out value="${"Ativo"}"/><td>
                        </c:when>
                        <c:otherwise>
                            <td><c:out value="${"Passado"}"/><td>
                        </c:otherwise>
                        </c:choose>

                    </tr>  
                </tbody>                     
                </c:forEach>
          </table>
                
             <br><br>
            <hr>
            
             
             <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
       </div>
           
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${participanteLogado.tipo == 'admin'}">
                     <div class="w3-content" style="max-width:1200px">
            <h2> Eventos Atuais</h2>
            <table  class="w3-table-all">
                <thead>
                    <th>Título</th>
                    <th>Entidade Promotora</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                     <th>Valor</th>
                    <th>Status</th>
                   
                   
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                        
                        <td><c:out value="${u.entidade.nome_entidade}"/></td>
                        <td><c:out value="${u.dt_inicio}"/></td>
                        <td><c:out value="${u.dt_final}"/></td>
                        <td>R$ <c:out value="${u.preco}"/></td>
                        <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td><c:out value="${"Ativo"}"/><td>
                        </c:when>
                        <c:otherwise>
                            <td><c:out value="${"Passado"}"/><td>
                        </c:otherwise>
                        </c:choose>
                        
                        
                        
                        <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td>
                           <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/inscricao?id=${participanteLogado.uid}&id2=${u.uid}">
                                    Inscrição
                                </a>
                        </td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            
                        </c:otherwise>
                        </c:choose>
                        
                        <td>
                            <c:choose>
                                <c:when test="${u.liberado == '0'}">
                                    <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/libera?id=${u.uid}&op=1">
                                    Libera Inscrições
                                    </a>
                                    
                                </c:when> 
                                <c:otherwise>
                                    <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/libera?id=${u.uid}&op=0">
                                    Fecha Inscrições
                                    </a>
                                </c:otherwise>
                                
                            </c:choose>
                            
                        </td>
                        <td>
                                <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/update?id=${u.uid}" >
                                    Editar
                                </a>
                        </td>
                        <td>
                                <a class="btn btn-default link_excluir_evento" href="${pageContext.servletContext.contextPath}/evento/delete?id=${u.uid}">
                                    Excluir
                                </a>                             
                        </td>
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/inscricoes?id=${u.uid}&&op=${"nada"}" >
                                    Listar Inscritos
                                </a>
                        </td>
                        
                         <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/presencas?id=${u.uid}" >
                                    Listar Presentes
                                </a>
                        </td>
                        
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/renda?id=${u.uid}" >
                                    Renda Total
                                </a>
                        </td>
                        
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/estatisticas?id=${u.uid}" >
                                    Estatísticas
                                </a>
                        </td>
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/importainsc?id=${u.uid}" >
                                    Importar Inscrições
                                </a>
                        </td>
                </tbody>
                    </tr>                    
                </c:forEach>
                    
                
                
            </table>
                
             <br><br>
            <hr>
                    
                </c:when>
            
            <c:when test="${participanteLogado.tipo == 'membro'}">
                
                            <h2> Eventos Atuais</h2>
            <table  class="w3-table-all">
                <thead>
                    <th>Título</th>
                    <th>Entidade Promotora</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                     <th>Valor</th>
                    <th>Status</th>
                   
                   
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                        
                        <td><c:out value="${u.entidade.nome_entidade}"/></td>
                        <td><c:out value="${u.dt_inicio}"/></td>
                        <td><c:out value="${u.dt_final}"/></td>
                        <td>R$ <c:out value="${u.preco}"/></td>
                        <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td><c:out value="${"Ativo"}"/><td>
                        </c:when>
                        <c:otherwise>
                            <td><c:out value="${"Passado"}"/><td>
                        </c:otherwise>
                        </c:choose>
                        
                        
                        
                        <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td>
                           <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/inscricao?id=${participanteLogado.uid}&id2=${u.uid}">
                                    Inscrição
                                </a>
                        </td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            
                        </c:otherwise>
                        </c:choose>
                        
                        <td>
                            <c:choose>
                                <c:when test="${u.liberado == '0'}">
                                    <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/libera?id=${u.uid}&op=1">
                                    Libera Inscrições
                                    </a>
                                    
                                </c:when> 
                                <c:otherwise>
                                    <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/libera?id=${u.uid}&op=0">
                                    Fecha Inscrições
                                    </a>
                                </c:otherwise>
                                
                            </c:choose>
                            
                        </td>
                        <td>
                                <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/update?id=${u.uid}" >
                                    Editar
                                </a>
                        </td>
                        <td>
                                <a class="btn btn-default link_excluir_evento" href="${pageContext.servletContext.contextPath}/evento/delete?id=${u.uid}">
                                    Excluir
                                </a>                             
                        </td>
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/inscricoes?id=${u.uid}&&op=${"nada"}" >
                                    Listar Inscritos
                                </a>
                        </td>
                        
                         <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/presencas?id=${u.uid}" >
                                    Listar Presentes
                                </a>
                        </td>
                        
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/renda?id=${u.uid}" >
                                    Renda Total
                                </a>
                        </td>
                        
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/estatisticas?id=${u.uid}" >
                                    Estatísticas
                                </a>
                        </td>
                        <td>
                            <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/evento/importainsc?id=${u.uid}" >
                                    Importar Inscrições
                                </a>
                        </td>
                </tbody>
                    </tr>                    
                </c:forEach>
                    
                
                
            </table>
                
             <br><br>
            <hr>
                
            </c:when>
            
            <c:otherwise>
                
                          <div class="w3-content" style="max-width:1200px">
            <h2> Eventos Atuais</h2>
            <table  class="w3-table-all">
                <thead>
                    <th>Título</th>
                    <th>Entidade Promotora</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Valor</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                        
                        <td><c:out value="${u.entidade.nome_entidade}"/></td>
                        <td><c:out value="${u.dt_inicio}"/></td>
                        <td><c:out value="${u.dt_final}"/></td>
                        <td>R$ <c:out value="${u.preco}"/></td>
                         <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td><c:out value="${"Ativo"}"/><td>
                        </c:when>
                        <c:otherwise>
                            <td><c:out value="${"Passado"}"/><td>
                        </c:otherwise>
                        </c:choose>
                                
                                 <c:choose>
                        <c:when test="${u.liberado == 1}">
                            <td>
                           <a class="btn btn-default"  href="${pageContext.servletContext.contextPath}/evento/inscricao?id=${participanteLogado.uid}&id2=${u.uid}">
                                    Inscrição
                                </a>
                        </td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            
                        </c:otherwise>
                        </c:choose>

                    </tr>  
                </tbody>                     
                </c:forEach>
          </table>
                
             <br><br>
            <hr>
            
          </c:otherwise>
            
                
            </c:choose>
           

            <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
             
</div>
        </c:otherwise>
        </c:choose>
           

    </body>
</html>
