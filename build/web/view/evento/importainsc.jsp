
<%@include file="/view/include/loginCheck.jsp"%>
<%@page import="java.util.List"%>
<%@page import="model.Evento"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Importa Inscrições</title>
    </head>
    <body class="w3-light-grey">
       <div class="w3-content" style="max-width:1200px">
            <h2> Importar inscrições</h2>
           
            <c:set var="ident" scope="session" value="${evento}"/>
            <c:out value="${evento.titulo}"></c:out><h6> PARA: </h6>
            
            <form class="form-group" name="formImporta" method="POST" action="${pageContext.servletContext.contextPath}/evento/importainsc"> 
            <label class="h4">Evento</label>
            
                      <select name="id_evento"  >
                        <c:forEach var="x" items="${eventoList}">
                            <option value="${x.uid}">${x.titulo}</option>
                        </c:forEach>
                     </select>
            
               <button class="btn btn-lg btn-primary" type="submit">Importar</button>
            </form>
            <hr>
            <button><a href="${pageContext.servletContext.contextPath}/evento">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
        </div>
    </body>
</html>
