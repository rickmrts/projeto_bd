<%@include file="/view/include/loginCheck.jsp"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
       <title>Cadastro de Eventos</title>
    </head>

    <body  class="w3-light-grey">
        <div class="w3-content" style="max-width:1200px"">
            
            <h2 class="text-center">Novo Cadastro de evento</h2>

            <c:choose>
                <c:when test="${param.acao == 'c'}">
                <form class="form-group" name="formEvento" method="POST" action="${pageContext.servletContext.contextPath}/evento/create">                    
                </c:when>
                 <c:otherwise>
                <form class="form-group" name="formEvento" method="POST" action="${pageContext.servletContext.contextPath}/evento/update">                    
                    <input type="hidden" name="id" value="${u.uid}">
                </c:otherwise>    
               
            </c:choose>
                <c:set var="ident" scope="session" value="${u.entidade.id_entidade}"/>
                   <label class="h4">Titulo</label>
                    <input class="form-control" type="text" name="titulo" value="${u.titulo}" required autofocus>
                    <hr>
                    <label class="h4">Descrição</label>
                    <input class="form-control" type="text" name="descricao" value="${u.descricao}" required>
<hr>
                    <label class="h4">Informações Importantes</label>
                    <input class="form-control" type="text" name="informacoes_imp" value="${u.informacoes_imp}" required>
                      <hr>
                     <label class="h4">Local</label>
                      <select name="id_local"  >
                        <c:forEach var="x" items="${localList}">
                            <option value="${x.id_local}">${x.nome_local}</option>
                        </c:forEach>
                     </select>
                     <hr>
                     <label class="h4">Entidade</label>
                      <select name="id_entidade">
                       
                        <c:forEach var="x" items="${entidadeList}">
                            <c:choose>
                                <c:when test="${ident==x.id_entidade}">
                                    <option selected value="${x.id_entidade}">${x.nome_entidade}</option>
                                </c:when>   
                                    <c:otherwise>
                                        <option value="${x.id_entidade}">${x.nome_entidade}</option>
                                    </c:otherwise>
                                
                            </c:choose>
                            
                        </c:forEach>
                     </select>
                    <hr>
                    <label class="h4">Data Inicio</label>
                    <input class="form-control" type="date" name="dt_inicio" value="${u.dt_inicio}" required >
                    <hr>
                    <label class="h4">Data Final</label>
                    <input class="form-control" type="date" name="dt_final" value="${u.dt_final}" required >

                    <hr><label class="h4">Preço</label>
                    <input class="form-control" type="text" name="preco" value="${u.preco}" required >
       
                    <c:choose>
                    <c:when test="${param.acao == 'c'}">
                       
                        <hr><button class="btn btn-lg btn-primary" type="submit">Cadastrar</button>
                     </c:when>
                     <c:otherwise>
                     
                     <hr><button class="btn btn-lg btn-primary" type="submit">Atualizar</button>  
                   
                    </c:otherwise>
           
                     </c:choose>
                    

   
            </form>
                    <br><button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
                    <hr>
        </div>
    </body>
</html>
