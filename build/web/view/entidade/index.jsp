<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page import="model.Participante"%>
<%@page import="model.EntidadePromotora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <style>
        body{
            font-family: sans-serif;
            font-size:large;
            margin-top:0%;
            margin-bottom: 60%;
            margin-left:20%;
            margin-right: 20%;
            background:lightblue;
        }
        
        h2{
            border: 1px solid black;
            border-radius:5px ;
            
        }
        button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
        }
       a:link, a:visited {
            background-color: green;
            color: white;
            padding: 10px 15px;
            text-align: center; 
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: blue;
        }
        th, td {
            padding: 15px;
            text-align: left;
            border-bottom: 1px solid #ddd_telefone;
        }
        </style>
        <title>Entidades</title>
    </head>
    <body>
      
        <div class="container">
            
            <table>
                <thead>
                    <th>Id </th>
                    <th>Nome Local</th>
                    <th>Endereco</th>
                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${entidadeList}">
                    <tr>
                        <td><c:out value="${u.id_entidade}"/></td>
                        
                        <td><c:out value="${u.nome_entidade}"/></td>
                        <td><c:out value="${u.descricao_entidade}"/></td>

                    </tr>                    
                </c:forEach>
                    
                </tbody>
            </table>
            
            <br><br>
            <hr>

             <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
             <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
      </div>
    </body>
</html>
