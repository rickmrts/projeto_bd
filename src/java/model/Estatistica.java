/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author rykka
 */
public class Estatistica {
    
    private int total;
    private int psexo[];
    private int fx_et[];
    private int sex_fx_et_m[];
    private int sex_fx_et_f[];
    private List<Inst_Orig> inst;

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the psexo
     */
    public int[] getPsexo() {
        return psexo;
    }

    /**
     * @param psexo the psexo to set
     */
    public void setPsexo(int[] psexo) {
        this.psexo = psexo;
    }

    /**
     * @return the fx_et
     */
    public int[] getFx_et() {
        return fx_et;
    }

    /**
     * @param fx_et the fx_et to set
     */
    public void setFx_et(int[] fx_et) {
        this.fx_et = fx_et;
    }

    /**
     * @return the sex_fx_et_m
     */
    public int[] getSex_fx_et_m() {
        return sex_fx_et_m;
    }

    /**
     * @param sex_fx_et_m the sex_fx_et_m to set
     */
    public void setSex_fx_et_m(int[] sex_fx_et_m) {
        this.sex_fx_et_m = sex_fx_et_m;
    }

    /**
     * @return the sex_fx_et_f
     */
    public int[] getSex_fx_et_f() {
        return sex_fx_et_f;
    }

    /**
     * @param sex_fx_et_f the sex_fx_et_f to set
     */
    public void setSex_fx_et_f(int[] sex_fx_et_f) {
        this.sex_fx_et_f = sex_fx_et_f;
    }

    /**
     * @return the inst
     */
    public List<Inst_Orig> getInst() {
        return inst;
    }

    /**
     * @param inst the inst to set
     */
    public void setInst(List<Inst_Orig> inst) {
        this.inst = inst;
    }




}
