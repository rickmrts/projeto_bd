/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rykka
 */
public class EntidadePromotora {
    private int id_entidade;
    private String nome_entidade;
    private String descricao_entidade;

    /**
     * @return the id_entidade
     */
    public int getId_entidade() {
        return id_entidade;
    }

    /**
     * @param id_entidade the id_entidade to set
     */
    public void setId_entidade(int id_entidade) {
        this.id_entidade = id_entidade;
    }

    /**
     * @return the nome_entidade
     */
    public String getNome_entidade() {
        return nome_entidade;
    }

    /**
     * @param nome_entidade the nome_entidade to set
     */
    public void setNome_entidade(String nome_entidade) {
        this.nome_entidade = nome_entidade;
    }

    /**
     * @return the descricao_entidade
     */
    public String getDescricao_entidade() {
        return descricao_entidade;
    }

    /**
     * @param descricao_entidade the descricao_entidade to set
     */
    public void setDescricao_entidade(String descricao_entidade) {
        this.descricao_entidade = descricao_entidade;
    }

}
