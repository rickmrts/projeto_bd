/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rykka
 */
    public class Inst_Orig{
        private int qtd;
        private String inst;

        /**
         * @return the qtd
         */
        public int getQtd() {
            return qtd;
        }

        /**
         * @param qtd the qtd to set
         */
        public void setQtd(int qtd) {
            this.qtd = qtd;
        }

        /**
         * @return the inst
         */
        public String getInst() {
            return inst;
        }

        /**
         * @param inst the inst to set
         */
        public void setInst(String inst) {
            this.inst = inst;
        }
        
        
    }
    