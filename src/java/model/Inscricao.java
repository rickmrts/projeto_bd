/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rykka
 */
public class Inscricao {
    
    private int id_participante;
    private int id_evento;
    private java.sql.Date data;
    private int pago;
    private int presenca;
    private String forma_pagamento;
   
    /**
     * @return the id_participante
     */
    public int getId_participante() {
        return id_participante;
    }

    /**
     * @param id_participante the id_participante to set
     */
    public void setId_participante(int id_participante) {
        this.id_participante = id_participante;
    }

    /**
     * @return the id_evento
     */
    public int getId_evento() {
        return id_evento;
    }

    /**
     * @param id_evento the id_evento to set
     */
    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

    /**
     * @return the data
     */
    public java.sql.Date getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(java.sql.Date data) {
        this.data = data;
    }

    /**
     * @return the pago
     */
    public int getPago() {
        return pago;
    }

    /**
     * @param pago the pago to set
     */
    public void setPago(int pago) {
        this.pago = pago;
    }

    /**
     * @return the presenca
     */
    public int getPresenca() {
        return presenca;
    }

    /**
     * @param presenca the presenca to set
     */
    public void setPresenca(int presenca) {
        this.presenca = presenca;
    }

    /**
     * @return the forma_pagamento
     */
    public String getForma_pagamento() {
        return forma_pagamento;
    }

    /**
     * @param forma_pagamento the forma_pagamento to set
     */
    public void setForma_pagamento(String forma_pagamento) {
        this.forma_pagamento = forma_pagamento;
    }
    
}
