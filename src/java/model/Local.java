/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rykka
 */
public class Local {

    private int id_local;
    private String nome_local;
    private String endereco;
    private String ddd;
    private String tel_telefone;
    private String tipo_telefone;
    private String coord_geo;

    /**
     * @return the nome_local
     */
    public String getNome_local() {
        return nome_local;
    }

    /**
     * @param nome_local the nome_local to set
     */
    public void setNome_local(String nome_local) {
        this.nome_local = nome_local;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

   
    /**
     * @return the coord_geo
     */
    public String getCoord_geo() {
        return coord_geo;
    }

    /**
     * @param coord_geo the coord_geo to set
     */
    public void setCoord_geo(String coord_geo) {
        this.coord_geo = coord_geo;
    }

    /**
     * @return the id_local
     */
    public int getId_local() {
        return id_local;
    }

    /**
     * @param id_local the id_local to set
     */
    public void setId_local(int id_local) {
        this.id_local = id_local;
    }

    /**
     * @return the ddd
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * @param ddd the ddd to set
     */
    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    /**
     * @return the tel_telefone
     */
    public String getTel_telefone() {
        return tel_telefone;
    }

    /**
     * @param tel_telefone the tel_telefone to set
     */
    public void setTel_telefone(String tel_telefone) {
        this.tel_telefone = tel_telefone;
    }

    /**
     * @return the tipo_telefone
     */
    public String getTipo_telefone() {
        return tipo_telefone;
    }

    /**
     * @param tipo_telefone the tipo_telefone to set
     */
    public void setTipo_telefone(String tipo_telefone) {
        this.tipo_telefone = tipo_telefone;
    }



}
