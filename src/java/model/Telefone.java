/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author rykka
 */
public class Telefone {
    private int id_telefone;
    private String ddd;
    private String tel_telefone;
    private String tipo_telefone;

    /**
     * @return the ddd
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * @param ddd the ddd to set
     */
    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    /**
     * @return the tel_telefone
     */
    public String getTel_telefone() {
        return tel_telefone;
    }

    /**
     * @param tel_telefone the tel_telefone to set
     */
    public void setTel_telefone(String tel_telefone) {
        this.tel_telefone = tel_telefone;
    }

    /**
     * @return the tipo_telefone
     */
    public String getTipo_telefone() {
        return tipo_telefone;
    }

    /**
     * @param tipo_telefone the tipo_telefone to set
     */
    public void setTipo_telefone(String tipo_telefone) {
        this.tipo_telefone = tipo_telefone;
    }

    /**
     * @return the id_telefone
     */
    public int getId_telefone() {
        return id_telefone;
    }

    /**
     * @param id_telefone the id_telefone to set
     */
    public void setId_telefone(int id_telefone) {
        this.id_telefone = id_telefone;
    }


    
}
