/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.sql.Date;

/**
 *
 * @author rykka
 */
public class Evento {
    private long uid;
    private String titulo;
    private String descricao;
    private String informacoes_imp;
    private EntidadePromotora entidade;
    private Local local;
    private Date dt_inicio;
    private Date dt_final;
    private int preco;
    private String pagamento;
    private int liberado;

    /**
     * @return the uid
     */
    public long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(long uid) {
        this.uid = uid;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the informacoes_imp
     */
    public String getInformacoes_imp() {
        return informacoes_imp;
    }

    /**
     * @param informacoes_imp the informacoes_imp to set
     */
    public void setInformacoes_imp(String informacoes_imp) {
        this.informacoes_imp = informacoes_imp;
    }


    /**
     * @return the pagamento
     */
    public String getPagamento() {
        return pagamento;
    }

    /**
     * @param pagamento the pagamento to set
     */
    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    /**
     * @return the entidade
     */
    public EntidadePromotora getEntidade() {
        return entidade;
    }

    /**
     * @param entidade the entidade to set
     */
    public void setEntidade(EntidadePromotora entidade) {
        this.entidade = entidade;
    }

    /**
     * @return the local
     */
    public Local getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * @return the preco
     */
    public int getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(int preco) {
        this.preco = preco;
    }

    /**
     * @return the dt_inicio
     */
    public Date getDt_inicio() {
        return dt_inicio;
    }

    /**
     * @param dt_inicio the dt_inicio to set
     */
    public void setDt_inicio(Date dt_inicio) {
        this.dt_inicio = dt_inicio;
    }

    /**
     * @return the dt_final
     */
    public Date getDt_final() {
        return dt_final;
    }

    /**
     * @param dt_final the dt_final to set
     */
    public void setDt_final(Date dt_final) {
        this.dt_final = dt_final;
    }

    /**
     * @return the liberado
     */
    public int getLiberado() {
        return liberado;
    }

    /**
     * @param liberado the liberado to set
     */
    public void setLiberado(int liberado) {
        this.liberado = liberado;
    }

  
}
