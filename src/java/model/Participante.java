/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.InputStream;
import java.sql.Date;
import java.util.Comparator;

/**
 *
 * @author dskaster
 */
public class Participante extends Telefone {
    
    private long uid;
    private String login;
    private String nome_completo;
    private String senha;
    private InputStream foto;
    private String sexo;
    private String cpf;
    private String rg;
    private String nome_cracha;
    private String email;
    private String logradouro;
    private String complemento;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;
    private Date data_nasc;
    private String estado_civil;
    private String escolaridade;
    private String profissao;
    private String inst_orig;
    private String como_fic_saben;
    private String outra_pessoa;
    private Telefone telefone;
    private String tipo;
    private Inscricao inscricao;
    private String forma_pagamento;
    private int image_id;


    /**
     * @return the uid
     */
    public long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(long uid) {
        this.uid = uid;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the nome_completo
     */
    public String getNome_completo() {
        return nome_completo;
    }

    /**
     * @param nome_completo the nome_completo to set
     */
    public void setNome_completo(String nome_completo) {
        this.nome_completo = nome_completo;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }


    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the rg
     */
    public String getRg() {
        return rg;
    }

    /**
     * @param rg the rg to set
     */
    public void setRg(String rg) {
        this.rg = rg;
    }

    /**
     * @return the nome_cracha
     */
    public String getNome_cracha() {
        return nome_cracha;
    }

    /**
     * @param nome_cracha the nome_cracha to set
     */
    public void setNome_cracha(String nome_cracha) {
        this.nome_cracha = nome_cracha;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the logradouro
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * @param logradouro the logradouro to set
     */
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * @return the complemento
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * @param complemento the complemento to set
     */
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(String cep) {
        this.cep = cep;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

 

    /**
     * @return the estado_civil
     */
    public String getEstado_civil() {
        return estado_civil;
    }

    /**
     * @param estado_civil the estado_civil to set
     */
    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }

    /**
     * @return the escolaridade
     */
    public String getEscolaridade() {
        return escolaridade;
    }

    /**
     * @param escolaridade the escolaridade to set
     */
    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    /**
     * @return the profissao
     */
    public String getProfissao() {
        return profissao;
    }

    /**
     * @param profissao the profissao to set
     */
    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    /**
     * @return the inst_orig
     */
    public String getInst_orig() {
        return inst_orig;
    }

    /**
     * @param inst_orig the inst_orig to set
     */
    public void setInst_orig(String inst_orig) {
        this.inst_orig = inst_orig;
    }

    /**
     * @return the como_fic_saben
     */
    public String getComo_fic_saben() {
        return como_fic_saben;
    }

    /**
     * @param como_fic_saben the como_fic_saben to set
     */
    public void setComo_fic_saben(String como_fic_saben) {
        this.como_fic_saben = como_fic_saben;
    }

    /**
     * @return the outra_pessoa
     */
    public String getOutra_pessoa() {
        return outra_pessoa;
    }

    /**
     * @param outra_pessoa the outra_pessoa to set
     */
    public void setOutra_pessoa(String outra_pessoa) {
        this.outra_pessoa = outra_pessoa;
    }

   
    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the telefone
     */
    public Telefone getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the inscricao
     */
    public Inscricao getInscricao() {
        return inscricao;
    }

    /**
     * @param inscricao the inscricao to set
     */
    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    /**
     * @return the forma_pagamento
     */
    public String getForma_pagamento() {
        return forma_pagamento;
    }

    /**
     * @param forma_pagamento the forma_pagamento to set
     */
    public void setForma_pagamento(String forma_pagamento) {
        this.forma_pagamento = forma_pagamento;
    }

    /**
     * @return the image_id
     */
    public int getImage_id() {
        return image_id;
    }

    /**
     * @param image_id the image_id to set
     */
    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public static Comparator<Participante> COMPARE_BY_NAME = new Comparator<Participante>() {
        public int compare(Participante one, Participante other) {
            return one.nome_completo.compareTo(other.nome_completo);
        }
    };

    /**
     * @return the data_nasc
     */
    public Date getData_nasc() {
        return data_nasc;
    }

    /**
     * @param data_nasc the data_nasc to set
     */
    public void setData_nasc(Date data_nasc) {
        this.data_nasc = data_nasc;
    }

    /**
     * @return the foto
     */
    public InputStream getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(InputStream foto) {
        this.foto = foto;
    }



}
