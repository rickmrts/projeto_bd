/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.ParticipanteDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Participante;
import dao.DAO;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import model.Evento;
/**
 *
 * @author dskaster
 */
@WebServlet(name = "LoginController", urlPatterns = {
    "/login",
    "/logout",
    ""})
public class LoginController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        switch (request.getServletPath()) {
            case "":
                verificaLogin(request, response);
                break;

            case "/logout":
                doLogout(request, response);
                break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        switch (request.getServletPath()) {
            case "/login":
                doLogin(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void verificaLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);//obtém a sessão sem forçar criá-la (false)
        RequestDispatcher dispatcher;
         List<Evento> eventoList = new LinkedList<>();
        
        if (session != null && session.getAttribute("participanteLogado") != null) {          
            dispatcher = request.getRequestDispatcher("/welcome.jsp");
        } else {
            
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            eventoList= dao.all();
            
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }
            request.setAttribute("eventoList", eventoList);
            dispatcher = request.getRequestDispatcher("/index.jsp");
        }
        
        dispatcher.forward(request, response);
    }
    
    public static byte[] getBytes(InputStream is) throws IOException {

            int len;
            int size = 1024;
            byte[] buf;

            if (is instanceof ByteArrayInputStream) {
              size = is.available();
              buf = new byte[size];
              len = is.read(buf, 0, size);
            } else {
              ByteArrayOutputStream bos = new ByteArrayOutputStream();
              buf = new byte[size];
              while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
              buf = bos.toByteArray();
            }
            return buf;
      }
    

    private void doLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {

        switch (request.getServletPath()) {
            case "/login":
                Participante participante = new Participante();
                participante.setLogin(request.getParameter("login"));
                participante.setSenha(request.getParameter("senha"));

                HttpSession session;
                session = request.getSession();
                
                 try (DAOFactory daoFactory = new DAOFactory();) {
                    ParticipanteDAO dao = daoFactory.getParticipanteDAO();//observe que o tipo é da subclasse (ParticipanteDAO) e não da interface/classe abstrata (DAO), pois o método authenticate só é definido na subclasse
                    
                    dao.authenticate(participante);

                    
                    session.setAttribute("participanteLogado", participante);
                } catch (ClassNotFoundException | IOException | SQLException | SecurityException ex) {
                    session.setAttribute("error", ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/");
        }
    }

    private void doLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);

        if (session != null) {
            session.invalidate();//Invalida a sessão; será retornada uma sessão nova no próximo getSession
        }

        response.sendRedirect(request.getContextPath() + "/");
    }

    

}
