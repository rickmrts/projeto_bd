/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAO;
import dao.DAOFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.EntidadePromotora;

/**
 *
 * @author rykka
 */
@WebServlet(name = "EntidadePromotoraController", urlPatterns = {"/entidade","/entidade/create","/entidade/update","/entidade/delete"})
public class EntidadePromotoraController extends HttpServlet {

   
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         RequestDispatcher dispatcher;
        
        switch (request.getServletPath()) {
            case "/entidade":
                listEntidades(request, response);
                break;

            case "/entidade/create":
                dispatcher = request.getRequestDispatcher("/view/entidade/create.jsp?acao=c");
                dispatcher.forward(request, response);
                break;

            case "/entidade/delete":
                deleteEntidades(request, response);
                break;

            case "/entidade/update":
                updateEntidades(request, response);
                break;
        }
        
    }

    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch (request.getServletPath()) {
            case "/entidade/create":
        {
            createEntidade(request, response);
        }
                 break;

            case "/entidade/update":
                updateEntidadePost(request, response);
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void createEntidade(HttpServletRequest request, HttpServletResponse response) throws IOException {
        EntidadePromotora entidade = new EntidadePromotora();
        
        entidade.setNome_entidade(request.getParameter("nome_entidade"));
        entidade.setDescricao_entidade(request.getParameter("descricao_entidade"));
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEntidadeDAO();

            dao.create(entidade);

            response.sendRedirect(request.getContextPath() + "/evento");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/entidade/create");
        }   
    }
    private void listEntidades(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEntidadeDAO();

            List<EntidadePromotora> entidadeList = dao.all();
            request.setAttribute("entidadeList", entidadeList);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/entidade/index.jsp");
        dispatcher.forward(request, response);      
    }

    private void deleteEntidades(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void updateEntidades(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    private void updateEntidadePost(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
