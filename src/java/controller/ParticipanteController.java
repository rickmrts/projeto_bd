package controller;

import dao.DAO;
import dao.DAOFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Evento;
import model.Participante;
import model.Telefone;

@WebServlet(
        name = "ParticipanteController",
        urlPatterns = {
            "/participante",
            "/participante/create",
            "/participante/delete",
            "/participante/update",
            "/participante/inscricoes",
            "/participante/cancelainsc",
            "/participante/pagamento",
            "/participante/presenca",
            "/participante/alterarpag",
            "/participante/getImage.action"})

@MultipartConfig(maxFileSize = 16177215)  

public class ParticipanteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher;

        switch (request.getServletPath()) {
            case "/participante":
                listParticipantes(request, response);
                break;

            case "/participante/create":
                dispatcher = request.getRequestDispatcher("/view/participante/create.jsp?acao=c");
                dispatcher.forward(request, response);
                break;

            case "/participante/delete":
                deleteParticipante(request, response);
                break;

            case "/participante/update":
                updateParticipante(request, response);
                break;
            case "/participante/inscricoes":
                listaInscricoes(request,response);
                break;
            case "/participante/cancelainsc":
                cancelaInscricao(request,response);
                break;
            case "/participante/pagamento":
                pagaEvento(request,response);
                break;
            case "/participante/presenca":
                presencaEvento(request,response);
                break;
                
            case "/participante/getImage.action":
            try (DAOFactory daoFactory = new DAOFactory();) {
                DAO dao = daoFactory.getParticipanteDAO();
                
                String imageID = request.getParameter("id");
                
                Participante p = new Participante();
                
                response.setContentType("image/jpg");
                
                p = (Participante) dao.read(Long.parseLong(imageID));
                
                InputStream imageData = p.getFoto();

                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16177215];

                while ((nRead = imageData.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }

                buffer.flush();
                

                response.getOutputStream().write(buffer.toByteArray());

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ParticipanteController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ParticipanteController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        switch (request.getServletPath()) {
            case "/participante/create":
        {
            try {
                createParticipante(request, response);
            } catch (ParseException ex) {
                Logger.getLogger(ParticipanteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;

            case "/participante/update":
        {
            try {
                updateParticipantePost(request, response);
            } catch (ParseException ex) {
                Logger.getLogger(ParticipanteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listParticipantes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();

            List<Participante> participanteList = dao.all();
            request.setAttribute("participanteList", participanteList);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/participante/index.jsp");
        dispatcher.forward(request, response);

    }

    private void createParticipante(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, ParseException {
        Participante participante = new Participante();
        Telefone telefone = new Telefone();
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date data_nasc = new java.sql.Date(format.parse(request.getParameter("data_nasc")).getTime());
                
        
        participante.setLogin(request.getParameter("login"));
        participante.setSenha(request.getParameter("senha"));
        participante.setNome_completo(request.getParameter("nome_completo"));
        participante.setCpf(request.getParameter("cpf"));
        participante.setRg(request.getParameter("rg"));
        participante.setEmail(request.getParameter("email"));
        participante.setNome_cracha(request.getParameter("nome_cracha"));
        participante.setLogradouro(request.getParameter("logradouro"));
        participante.setComplemento(request.getParameter("complemento"));
        participante.setBairro(request.getParameter("bairro"));
        participante.setCep(request.getParameter("cep"));
        participante.setCidade(request.getParameter("cidade"));
        participante.setEstado(request.getParameter("estado"));
        telefone.setTipo_telefone(request.getParameter("tipo_telefone"));
        telefone.setDdd(request.getParameter("ddd_telefone"));
        telefone.setTel_telefone(request.getParameter("tel_telefone"));
        participante.setTelefone(telefone);
        participante.setData_nasc(data_nasc);
        participante.setEstado_civil(request.getParameter("estado_civil"));
        participante.setEscolaridade(request.getParameter("escolaridade"));
        participante.setProfissao(request.getParameter("profissao"));
        participante.setInst_orig(request.getParameter("inst_orig"));
        participante.setComo_fic_saben(request.getParameter("como_fic_saben"));
        participante.setOutra_pessoa(request.getParameter("outra_pessoa"));
        participante.setTipo(request.getParameter("tipo"));
        participante.setSexo(request.getParameter("sexo"));
        
        InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("foto");
        if (filePart != null) {
            inputStream = filePart.getInputStream();
        }
        participante.setFoto(inputStream);
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();

            dao.create(participante);

            response.sendRedirect(request.getContextPath() + "/");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/participante/create");
        }
    }

    private void deleteParticipante(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();

            dao.delete(Long.parseLong(request.getParameter("id")));
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        response.sendRedirect(request.getContextPath() + "/participante");

    }

    private void updateParticipante(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();

            Participante usuario = (Participante) dao.read(Long.parseLong(request.getParameter("id")));
            request.setAttribute("u", usuario);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/participante/create.jsp?acao=u");
            dispatcher.forward(request, response);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/participante");
        }
    }

    private void updateParticipantePost(HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {
        Participante participante = new Participante();
        Telefone telefone = new Telefone();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date data_nasc = new java.sql.Date(format.parse(request.getParameter("data_nasc")).getTime());
         
        
        participante.setUid(Long.parseLong(request.getParameter("id")));
        participante.setLogin(request.getParameter("login"));
        participante.setNome_completo(request.getParameter("nome_completo"));
        participante.setCpf(request.getParameter("cpf"));
        participante.setRg(request.getParameter("rg"));
        participante.setEmail(request.getParameter("email"));
        participante.setNome_cracha(request.getParameter("nome_cracha"));
        participante.setLogradouro(request.getParameter("logradouro"));
        participante.setComplemento(request.getParameter("complemento"));
        participante.setBairro(request.getParameter("bairro"));
        participante.setCep(request.getParameter("cep"));
        participante.setCidade(request.getParameter("cidade"));
        participante.setEstado(request.getParameter("estado"));
        telefone.setTipo_telefone(request.getParameter("tipo_telefone"));
        telefone.setDdd(request.getParameter("ddd_telefone"));
        telefone.setTel_telefone(request.getParameter("tel_telefone"));
        participante.setTelefone(telefone);
        participante.setData_nasc(data_nasc);
        participante.setEstado_civil(request.getParameter("estado_civil"));
        participante.setEscolaridade(request.getParameter("escolaridade"));
        participante.setProfissao(request.getParameter("profissao"));
        participante.setInst_orig(request.getParameter("inst_orig"));
        participante.setComo_fic_saben(request.getParameter("como_fic_saben"));
        participante.setOutra_pessoa(request.getParameter("outra_pessoa"));
        participante.setTipo(request.getParameter("tipo"));
        participante.setSexo(request.getParameter("sexo"));

        if (!request.getParameter("senha").isEmpty()) {
            participante.setSenha(request.getParameter("senha"));
        }

        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();

            dao.update(participante);

            response.sendRedirect(request.getContextPath() + "/participante");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/participante/update");
        }
    }

    private void listaInscricoes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
           
            DAO dao = daoFactory.getEventoDAO();

            List<Evento> eventoList = dao.listaInscricoes(Integer.parseInt(request.getParameter("id")),"a");

            request.setAttribute("eventoList", eventoList);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/participante/inscricoes.jsp");
        dispatcher.forward(request, response);
    }

    private void cancelaInscricao(HttpServletRequest request, HttpServletResponse response) throws IOException {
            try (DAOFactory daoFactory = new DAOFactory();) {
                    DAO dao = daoFactory.getParticipanteDAO();

                    dao.cancelaInsc(Long.parseLong(request.getParameter("id")),Long.parseLong(request.getParameter("id2")));
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    request.getSession().setAttribute("error", ex.getMessage());
                }
            response.sendRedirect(request.getContextPath() + "/evento/list");
   }

    private void pagaEvento(HttpServletRequest request, HttpServletResponse response) throws IOException {
            try (DAOFactory daoFactory = new DAOFactory();) {
                    DAO dao = daoFactory.getParticipanteDAO();

                    dao.pagaEvento(Long.parseLong(request.getParameter("id")),Long.parseLong(request.getParameter("id2")));
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    request.getSession().setAttribute("error", ex.getMessage());
                }
            response.sendRedirect(request.getContextPath() + "/evento/list");    
    }

    private void presencaEvento(HttpServletRequest request, HttpServletResponse response) throws IOException {
            try (DAOFactory daoFactory = new DAOFactory();) {
                    DAO dao = daoFactory.getParticipanteDAO();

                    dao.presencaEvento(Long.parseLong(request.getParameter("id")),Long.parseLong(request.getParameter("id2")));
                } catch (ClassNotFoundException | IOException | SQLException ex) {
                    request.getSession().setAttribute("error", ex.getMessage());
                }
            response.sendRedirect(request.getContextPath() + "/evento/list");    
    }

}

   