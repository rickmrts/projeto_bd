/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAO;
import dao.DAOFactory;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Evento;
import model.EntidadePromotora;
import model.Estatistica;
import model.Local;
import model.Participante;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author rykka
 */
@WebServlet(name = "EventoController", urlPatterns = {"/evento","/evento/create","/evento/delete","/evento/update","/evento/list","/evento/inscricao","/evento/busca","/evento/inscricoes","/evento/presencas","/evento/renda","/evento/estatisticas","/evento/estatisticasm","/evento/libera","/evento/importainsc"})
public class EventoController extends HttpServlet {
    Long id1;
    Long id2;
    int id3;
     
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher dispatcher;
               
        switch (request.getServletPath()) {
            case "/evento":
        {
            try {
                listEventos(request, response);
            } catch (Exception ex) {
                Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
             break;

            case "/evento/create":
            try (DAOFactory daoFactory = new DAOFactory();) {
                DAO dao1 = daoFactory.getEntidadeDAO();
                DAO dao2 = daoFactory.getLocalDAO();
                List<EntidadePromotora> entidadeList = dao1.all();
                List<Local> localList = dao2.all();
                request.setAttribute("entidadeList",entidadeList);
                request.setAttribute("localList", localList);
                } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                dispatcher = request.getRequestDispatcher("/view/evento/create.jsp?acao=c");
                dispatcher.forward(request, response);
                break;

            case "/evento/delete":
                deleteEvento(request, response);
                break;

            case "/evento/update":
                updateEvento(request, response);
                break;
                
            case "/evento/inscricao":
                inscreveEvento(request,response);
                break;
             
            case "/evento/busca":
                dispatcher = request.getRequestDispatcher("/view/evento/busca.jsp?");
                dispatcher.forward(request, response);
                break;
            
            case "/evento/list":
        {
            try {
                listEventos(request, response);
            } catch (Exception ex) {
                Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
            case "/evento/inscricoes":
                listInscricoes(request, response);
                break;
                
            case "/evento/presencas":
                listPresencas(request,response);
                break;
                
            case "/evento/renda":
                calculaRenda(request,response);
                break;
            case "/evento/estatisticas":
                estatisticas(request,response);
                break;
            case "/evento/estatisticasm":
                try (DAOFactory daoFactory = new DAOFactory();) {
                DAO dao = daoFactory.getEventoDAO();
               
                List<Evento> eventoList = dao.all();
                request.setAttribute("eventoList",eventoList);
               
                } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                dispatcher = request.getRequestDispatcher("/view/evento/estatisticasm.jsp?");
                dispatcher.forward(request, response);
                break;
                
            case "/evento/libera":
                liberaEvento(request,response);
                break;
            case "/evento/importainsc":
                importaInsc(request,response);
                break;
        }    
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       switch (request.getServletPath()) {
            case "/evento/create":
       {
           try {
               createEvento(request, response);
           } catch (SQLException | ClassNotFoundException ex) {
               Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
           } catch (ParseException ex) {
               Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
                break;

            case "/evento/update":
       {
           try {
               updateEventoPost(request, response);
           } catch (ParseException ex) {
               Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
                break;
                
            case "/evento/busca":
       {
           try {
               busca(request,response);
           } catch (SQLException | ClassNotFoundException ex) {
               Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
           } catch (ParseException ex) {
               Logger.getLogger(EventoController.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
                break;
                
            case "/evento/inscricao":
                inscreveEventoPost(request,response,id1,id2);
                break;
            
            case "/evento/estatisticasm":
                estatisticasmPost(request,response);
                break;
                
            case "/evento/importainsc":
                importaInscPost(request,response,id3);
                break;

        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void listEventos(HttpServletRequest request, HttpServletResponse response) throws Exception {
         try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            List<Evento> eventoList = dao.all();
            request.setAttribute("eventoList", eventoList);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/index.jsp?");
        dispatcher.forward(request, response);
    }

    private void createEvento(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ClassNotFoundException, ServletException, ParseException {
                Evento evento = new Evento();
                EntidadePromotora entidade = new EntidadePromotora();
                Local local = new Local();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                java.sql.Date datai = new java.sql.Date(format.parse(request.getParameter("dt_inicio")).getTime());
                java.sql.Date dataf = new java.sql.Date(format.parse(request.getParameter("dt_final")).getTime());
                
                evento.setTitulo(request.getParameter("titulo"));
                evento.setDescricao(request.getParameter("descricao"));
                evento.setInformacoes_imp(request.getParameter("informacoes_imp"));
                entidade.setId_entidade(Integer.parseInt(request.getParameter("id_entidade")));
                local.setId_local(Integer.parseInt(request.getParameter("id_local")));
                evento.setEntidade(entidade);
                evento.setLocal(local);
                evento.setDt_inicio(datai);
                evento.setDt_final(dataf);
                evento.setPreco(Integer.parseInt(request.getParameter("preco")));
                evento.setLiberado(Integer.parseInt("0"));
        
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();
                        
            dao.create(evento);
            
            response.sendRedirect(request.getContextPath() + "/evento");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/evento/create");
        }  
        
    }

    private void deleteEvento(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            dao.delete(Long.parseLong(request.getParameter("id")));
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        response.sendRedirect(request.getContextPath() + "/evento");

    }

    private void updateEvento(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();
            DAO dao1 = daoFactory.getEntidadeDAO();
            DAO dao2 = daoFactory.getLocalDAO();
            
            Evento evento = (Evento) dao.read(Long.parseLong(request.getParameter("id")));
            List<EntidadePromotora> ent = (List<EntidadePromotora>) dao1.all();
            List<Local> loc = (List<Local>) dao2.all();
            
            request.setAttribute("u", evento);
            request.setAttribute("entidadeList", ent);
            request.setAttribute("localList", loc);
            

            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/create.jsp?acao=u");
            dispatcher.forward(request, response);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/evento");
        }
    }

    private void updateEventoPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ParseException {
        Evento  evento = new Evento();
        EntidadePromotora entidade = new EntidadePromotora();
        Local local = new Local();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date datai = new java.sql.Date(format.parse(request.getParameter("dt_inicio")).getTime());
        java.sql.Date dataf = new java.sql.Date(format.parse(request.getParameter("dt_final")).getTime());
                
        
        evento.setUid(Long.parseLong(request.getParameter("id")));
        evento.setTitulo(request.getParameter("titulo"));
        evento.setDescricao(request.getParameter("descricao"));
        evento.setInformacoes_imp(request.getParameter("informacoes_imp"));
        entidade.setId_entidade(Integer.parseInt(request.getParameter("id_entidade")));
        evento.setEntidade(entidade);
        local.setId_local(Integer.parseInt(request.getParameter("id_local")));
        evento.setLocal(local);
        evento.setDt_inicio(datai);
        evento.setDt_final(dataf);
        evento.setPreco(Integer.parseInt(request.getParameter("preco")));
        

   
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            dao.update(evento);

            response.sendRedirect(request.getContextPath() + "/evento");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/evento/update");
        }
     }

    private void inscreveEvento(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try (DAOFactory daoFactory = new DAOFactory();) {
            id1 = Long.parseLong(request.getParameter("id"));
            id2 = Long.parseLong(request.getParameter("id2"));
      
            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/inscricao.jsp?");
            dispatcher.forward(request, response);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/evento");
        }
    }
    
    private void inscreveEventoPost(HttpServletRequest request, HttpServletResponse response,long id1,long id2) throws IOException {
        String parse = request.getParameter("forma_pagamento");
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();
            
            dao.inscreve(id1,id2,parse);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        response.sendRedirect(request.getContextPath() + "/evento");
    }
    
    private void busca(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException, ClassNotFoundException, ParseException {
            Evento evento = new Evento();
            EntidadePromotora entidade = new EntidadePromotora();
            int op = 1;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date datai = new java.sql.Date(format.parse(request.getParameter("dt_inicio")).getTime());
            java.sql.Date dataf = new java.sql.Date(format.parse(request.getParameter("dt_final")).getTime());
          
            
            switch(request.getParameter("radios")){
                
                case "titulo":
                    evento.setTitulo(request.getParameter("palavra"));
                    op = 1;
                    break;
                    
                case "entidade_promotora":
                    entidade.setNome_entidade(request.getParameter("palavra"));
                    evento.setEntidade(entidade);
                    op = 2;
                    break;
                
                case "periodo":
                    evento.setDt_inicio(datai);
                    evento.setDt_final(dataf);
                    op = 3;
                    break;
            }

            try(DAOFactory daoFactory = new DAOFactory();){
                DAO dao = daoFactory.getEventoDAO();
                List<Evento> eventoList = dao.busca(evento,op);
                request.setAttribute("eventoList", eventoList);

            } catch (ClassNotFoundException | IOException | SQLException ex) {
                request.getSession().setAttribute("error", ex.getMessage());
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/buscar.jsp");
            dispatcher.forward(request, response);
} 

    private void listInscricoes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (DAOFactory daoFactory = new DAOFactory();) {
           
            DAO dao = daoFactory.getParticipanteDAO();
   
            List<Participante> participantesList = dao.listaInscricoes(Integer.parseInt(request.getParameter("id")),request.getParameter("op"));

            request.setAttribute("participantesList", participantesList);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/inscricoes/index.jsp");
        dispatcher.forward(request, response);
    }

    private void listPresencas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try (DAOFactory daoFactory = new DAOFactory();) {

                DAO dao = daoFactory.getParticipanteDAO();

                List<Participante> participantesList = dao.listaPresentes(Integer.parseInt(request.getParameter("id")));

                request.setAttribute("participantesList", participantesList);

            } catch (ClassNotFoundException | IOException | SQLException ex) {
                request.getSession().setAttribute("error", ex.getMessage());
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/presencas.jsp");
            dispatcher.forward(request, response);    
    }

    private void calculaRenda(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try (DAOFactory daoFactory = new DAOFactory();) {

                DAO dao = daoFactory.getEventoDAO();
                Integer v = 0;

                List<Integer> renda = dao.calcula(Integer.parseInt(request.getParameter("id")));

              for (Iterator iterator = renda.iterator(); iterator.hasNext(); ) {  
                        Integer valor =  (Integer) iterator.next();  
                        v += valor;
                        
              }
               
              request.setAttribute("renda", v);

            } catch (ClassNotFoundException | IOException | SQLException ex) {
                request.getSession().setAttribute("error", ex.getMessage());
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/renda.jsp");
            dispatcher.forward(request, response);        
    }

    private void estatisticas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try(DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            Estatistica estatisticas = dao.estatisticas(Integer.parseInt(request.getParameter("id")));
           
            request.setAttribute("estatisticas", estatisticas);
       }
       catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
       }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/estatisticas.jsp");
        dispatcher.forward(request, response);      
    }

   
    private void estatisticasmPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String evs[] = request.getParameterValues("uid");
        int i = 0;
        
        List <Evento> res = new ArrayList<Evento>();

        
        try (DAOFactory daoFactory = new DAOFactory();) {
           
            DAO dao = daoFactory.getEventoDAO();
           
            res = dao.estatisticasm(evs);
        
             
        response.setContentType("image/png");

            try (ServletOutputStream os = response.getOutputStream()) {
                DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                 
                 
                for(Evento e: res) {
                    Estatistica estatisticas = new Estatistica();
                    estatisticas=dao.estatisticas((int) e.getUid());
                    
                    dataset.addValue(estatisticas.getTotal(),e.getTitulo(),e.getDt_inicio());
                    
                    System.err.println(estatisticas.getTotal());
                }
                
                JFreeChart chart = ChartFactory.createBarChart("Inscrições", "",
                        "Quantidade", dataset, PlotOrientation.VERTICAL, true, true, false);
            
                RenderedImage chartImage = chart.createBufferedImage(400, 400);
                ImageIO.write(chartImage, "png", os);
                os.flush();
            }
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }
        
    }


    private void liberaEvento(HttpServletRequest request, HttpServletResponse response) throws IOException {
          int id = Integer.parseInt(request.getParameter("id"));
          int op = Integer.parseInt(request.getParameter("op"));
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();
            
            dao.libera(id,op);
            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        response.sendRedirect(request.getContextPath() + "/evento");    
    
    }

    private void importaInsc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getEventoDAO();

            List<Evento> eventoList = dao.all();
            request.setAttribute("eventoList", eventoList);
            request.setAttribute("evento", dao.retorna(id));

            
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }
        id3 = id;

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/evento/importainsc.jsp?");
        dispatcher.forward(request, response);
    }
    

    private void importaInscPost(HttpServletRequest request, HttpServletResponse response,int id3) throws IOException {
       
        int id4 = Integer.parseInt(request.getParameter("id_evento"));
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getParticipanteDAO();
            
            List<Participante> ev = dao.listaInscricoes(id3, "nada");
            
            dao.importaInsc(ev,id4);
            
            response.sendRedirect(request.getContextPath() + "/evento");
           
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/evento/create");
        }  
 
    }
    
    
    
 
}