/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAO;
import dao.DAOFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Local;
import model.Telefone;

/**
 *
 * @author rykka
 */
@WebServlet(name = "LocalController", urlPatterns = {"/local",
                                                     "/local/create", 
                                                     "/local/delete",
                                                     "/local/update"})
public class LocalController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher dispatcher;
        
        switch (request.getServletPath()) {
            case "/local":
                listLocais(request, response);
                break;

            case "/local/create":
                dispatcher = request.getRequestDispatcher("/view/local/create.jsp?");
                dispatcher.forward(request, response);
                break;

            case "/local/delete":
                deleteLocais(request, response);
                break;

            case "/local/update":
                updateLocais(request, response);
                break;
        }
       
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        switch (request.getServletPath()) {
            case "/local/create":
        {
            try {
                createLocal(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(LocalController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                 break;

            case "/local/update":
                updateLocalPost(request, response);
                break;
        }
        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


    private void createLocal(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        Local local = new Local();
        Telefone telefone = new Telefone();
        
        //telefone.setTipo_telefone(request.getParameter("tipo_telefone"));
        //telefone.setDdd(request.getParameter("ddd_telefone"));
        //telefone.setTel_telefone(request.getParameter("tel_telefone"));
        //telefone.setId_telefone(Integer.parseInt(request.getParameter("id_telefone")));
        //local.setTelefone(telefone);
        local.setNome_local(request.getParameter("nome_local"));
        local.setEndereco(request.getParameter("endereco_local"));
        local.setCoord_geo(request.getParameter("coord_geo"));
        
        try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getLocalDAO();

            dao.create(local);

            response.sendRedirect(request.getContextPath() + "/local");
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            HttpSession session = request.getSession();
            session.setAttribute("error", ex.getMessage());
            response.sendRedirect(request.getContextPath() + "/local/create");
        }   
    }
    
    private void listLocais(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     try (DAOFactory daoFactory = new DAOFactory();) {
            DAO dao = daoFactory.getLocalDAO();

            List<Local> localList = dao.all();
            request.setAttribute("localList", localList);
        } catch (ClassNotFoundException | IOException | SQLException ex) {
            request.getSession().setAttribute("error", ex.getMessage());
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/view/local/index.jsp");
        dispatcher.forward(request, response);    
    }

    private void deleteLocais(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void updateLocais(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    private void updateLocalPost(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
