/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import model.Estatistica;
import model.Evento;
import model.Participante;


/**
 *
 * @author dskaster
 */
public abstract class DAO<T> {

    protected Connection connection;

    DAO(Connection connection) {
        this.connection = connection;
    }

    public abstract void create(T t) throws SQLException;
    public abstract T read(Long id) throws SQLException;
    public abstract void update(T t) throws SQLException;
    public abstract void delete(Long id) throws SQLException;

    public abstract List<T> all() throws SQLException;

    public abstract void inscreve(long id,long id2,String parse);

    public abstract List<T> busca(Evento ev,int op) throws SQLException;
    
    public abstract void cancelaInsc(long parseLong,long parseLong0);

    public abstract void pagaEvento(long parseLong, long parseLong0);

    public abstract void presencaEvento(long parseLong, long parseLong0);

    public abstract List<T> listaPresentes(int parseInt);

    public abstract T retorna(int id_entidade);

    public abstract List<Integer> calcula(int parseInt);

    public abstract List<T> listaInscricoes(int parseInt, String parameter) throws SQLException;

    public abstract Estatistica estatisticas(int parseInt);

    public abstract List<T> estatisticasm(String[] evs);

    public abstract  void libera(int id,int op);

    public abstract void importaInsc(List<Participante> ev, int id4);
    
}
