package dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Estatistica;
import model.Evento;
import model.Inscricao;
import model.Participante;
import model.Telefone;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;



public class ParticipanteDAO extends DAO<Participante> {

    static String allQuery = "SELECT uid, login, senha, nome_completo, cpf, rg, email, nome_cracha, \n" +
"       logradouro, complemento, bairro, cep, cidade, estado, tipo_telefone, \n" +
"       ddd_telefone, tel_telefone, data_nasc, estado_civil, escolaridade, \n" +
"       profissao, inst_orig, como_fic_saben, outra_pessoa,foto,tipo,sexo,image_id FROM participante";

    static String createQuery = "INSERT INTO participante(login, senha, nome_completo, cpf, rg, email, nome_cracha, \n" +
"            logradouro, complemento, bairro, cep, cidade, estado, tipo_telefone, \n" +
"            ddd_telefone, tel_telefone, data_nasc, estado_civil, escolaridade, \n" +
"            profissao, inst_orig, como_fic_saben, outra_pessoa, foto, tipo, \n" +
"            sexo,image_id) VALUES (?, md5(?), ?, ?, ?, ?, ?, ?, \n" +
"            ?, ?, ?, ?, ?, ?, \n" +
"            ?, ?, ?, ?, ?, \n" +
"            ?, ?, ?, ?, ?, ?, \n" +
"            ?,?) RETURNING uid";

    static String deleteQuery = "DELETE FROM participante WHERE uid=?";

    static String readQuery = "SELECT login, senha, nome_completo, cpf, rg, email, nome_cracha, \n" +
"       logradouro, complemento, bairro, cep, cidade, estado, tipo_telefone, \n" +
"       ddd_telefone, tel_telefone, data_nasc, estado_civil, escolaridade, \n" +
"       profissao, inst_orig, como_fic_saben, outra_pessoa,foto,tipo,sexo,image_id FROM participante WHERE uid=?";
    
    static String updateQuery = "UPDATE participante SET login=?, nome_completo=?, cpf=?, rg=?, email=?, \n" +
"       nome_cracha=?, logradouro=?, complemento=?, bairro=?, cep=?, \n" +
"       cidade=?, estado=?, tipo_telefone=?, ddd_telefone=?, tel_telefone=?, \n" +
"       data_nasc=?, estado_civil=?, escolaridade=?, profissao=?, inst_orig=?, \n" +
"       como_fic_saben=?, outra_pessoa=?,tipo=?, sexo=? WHERE uid=?";

    static String updateWithPasswordQuery = "UPDATE participante SET login=?, nome_completo=?, cpf=?, rg=?, email=?, \n" +
"       nome_cracha=?, logradouro=?, complemento=?, bairro=?, cep=?, \n" +
"       cidade=?, estado=?, tipo_telefone=?, ddd_telefone=?, tel_telefone=?, \n" +
"       data_nasc=?, estado_civil=?, escolaridade=?, profissao=?, inst_orig=?, \n" +
"       como_fic_saben=?, outra_pessoa=?, tipo=?, sexo=?, senha=md5(?) WHERE uid=?";
    
    static String authenticateQuery = "SELECT uid, nome_completo,tipo,foto FROM participante WHERE login=? AND senha=md5(?)";

    static String inscreveQuery = "INSERT INTO inscricoes(id_participante,id_evento,pago,data_inscricao,presenca,forma_pagamento) VALUES (?,?,?,?,?,?)";

    static String readInscreveQuery = "SELECT * FROM inscricoes WHERE id_evento=?";
    
    static String readInscreveQuerydt = "SELECT * FROM inscricoes WHERE id_evento=? ORDER BY data_inscricao ";
    
    static String readInscreveQueryst = "SELECT * FROM inscricoes WHERE id_evento=? ORDER BY pago";
    
    static String searchP = "SELECT * FROM participante WHERE uid=?";
   
    static String cancelaInsc = "DELETE FROM inscricoes WHERE id_evento=? AND id_participante=?";
    
    static String pagaInsc = "UPDATE inscricoes SET pago=1 WHERE id_evento=? AND id_participante=?";
    
    static String confirmaPres = "UPDATE inscricoes SET presenca=1 WHERE id_evento=? AND id_participante=?";
    
    static String listaPresentes = "SELECT * FROM inscricoes WHERE presenca=1 AND id_evento=?";
    
    public ParticipanteDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Participante p) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {

            statement.setString(1,p.getLogin());
            statement.setString(2,p.getSenha());
            statement.setString(3, p.getNome_completo());
            statement.setString(4, p.getCpf());
            statement.setString(5, p.getRg());
            statement.setString(6, p.getEmail());
            statement.setString(7,p.getNome_cracha());
            statement.setString(8,p.getLogradouro());
            statement.setString(9,p.getComplemento());
            statement.setString(10,p.getBairro());
            statement.setString(11,p.getCep());
            statement.setString(12,p.getCidade());
            statement.setString(13,p.getEstado());
            statement.setString(14,p.getTelefone().getTipo_telefone());
            statement.setString(15,p.getTelefone().getDdd());
            statement.setString(16,p.getTelefone().getTel_telefone());
            statement.setDate(17, p.getData_nasc());
            statement.setString(18, p.getEstado_civil());
            statement.setString(19, p.getEscolaridade());
            statement.setString(20, p.getProfissao());       
            statement.setString(21, p.getInst_orig());
            statement.setString(22,p.getComo_fic_saben());
            statement.setString(23,p.getOutra_pessoa());
            statement.setBinaryStream(24, p.getFoto());
            statement.setString(25,p.getTipo());
            statement.setString(26,p.getSexo());
            statement.setInt(27, (int) p.getUid());
            
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    p.setUid(result.getLong("uid"));
                }
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uk_usuario_login")) {
                throw new SQLException("Erro ao inserir usuário: login já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao inserir usuário: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao inserir usuário.");
            }
        }
    }

    @Override
    public Participante read(Long id) throws SQLException {
        Participante participante = new Participante();
        Telefone telefone = new Telefone();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setLong(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    participante.setUid(id);
                    participante.setLogin(result.getString("login"));
                    participante.setSenha(result.getString("senha"));
                    participante.setNome_completo(result.getString("nome_completo"));
                    participante.setCpf(result.getString("cpf"));
                    participante.setRg(result.getString("rg"));
                    participante.setEmail(result.getString("email"));
                    participante.setNome_cracha(result.getString("nome_cracha"));
                    participante.setLogradouro(result.getString("logradouro"));
                    participante.setComplemento(result.getString("complemento"));
                    participante.setBairro(result.getString("bairro"));
                    participante.setCep(result.getString("cep"));
                    participante.setCidade(result.getString("cidade"));
                    participante.setEstado(result.getString("estado"));
                    telefone.setTipo_telefone(result.getString("tipo_telefone"));
                    telefone.setDdd(result.getString("ddd_telefone"));
                    telefone.setTel_telefone(result.getString("tel_telefone"));
                    participante.setTelefone(telefone);    
                    participante.setData_nasc(result.getDate("data_nasc"));
                    participante.setEstado_civil(result.getString("estado_civil"));
                    participante.setEscolaridade(result.getString("escolaridade"));
                    participante.setProfissao(result.getString("profissao"));
                    participante.setInst_orig(result.getString("inst_orig"));
                    participante.setComo_fic_saben(result.getString("como_fic_saben"));
                    participante.setOutra_pessoa(result.getString("outra_pessoa"));
                    participante.setTipo(result.getString("tipo"));
                    participante.setFoto(result.getBinaryStream("foto"));
                    participante.setSexo(result.getString("sexo"));
                    participante.setImage_id((int)(long)id);
                    
                } else {
                    throw new SQLException("Erro ao visualizar: usuário não encontrado.");
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao visualizar: usuário não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao visualizar usuário.");
            }
        }
        return participante;
    }

    @Override
    public void update(Participante participante) throws SQLException {
        String query;

        if (participante.getSenha() == null) {
            query = updateQuery;
        } else {
            query = updateWithPasswordQuery;
        }

        try (PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setString(1, participante.getLogin());
            statement.setString(2, participante.getNome_completo());
            statement.setString(3, participante.getCpf());
            statement.setString(4, participante.getRg());
            statement.setString(5, participante.getEmail());
            statement.setString(6, participante.getNome_cracha());
            statement.setString(7,participante.getLogradouro());
            statement.setString(8,participante.getComplemento());
            statement.setString(9,participante.getBairro());
            statement.setString(10,participante.getCep());
            statement.setString(11,participante.getCidade());
            statement.setString(12,participante.getEstado());
            statement.setString(13,participante.getTelefone().getTipo_telefone());
            statement.setString(14,participante.getTelefone().getDdd());
            statement.setString(15,participante.getTelefone().getTel_telefone());
            statement.setDate(16,participante.getData_nasc());
            statement.setString(17,participante.getEstado_civil());
            statement.setString(18, participante.getEscolaridade());
            statement.setString(19, participante.getProfissao());       
            statement.setString(20, participante.getInst_orig());
            statement.setString(21,participante.getComo_fic_saben());
            statement.setString(22,participante.getOutra_pessoa());
            statement.setString(23,participante.getTipo());
            statement.setString(24,participante.getSexo());
            statement.setLong(25, participante.getUid());
            
            if(participante.getSenha()!=null){
                statement.setString(26, participante.getSenha());
            }
            
            
           if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao editar: usuário não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao editar: usuário não encontrado.")) {
                throw ex;
            } else if (ex.getMessage().contains("uk_usuario_login")) {
                throw new SQLException("Erro ao editar usuário: login já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao editar usuário: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao editar usuário.");
            }
        }
    }

    @Override
    public void delete(Long id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setLong(1, id);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao excluir: usuário não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao excluir: usuário não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao excluir usuário.");
            }
        }

    }

    @Override
    public List<Participante> all() throws SQLException {
        List<Participante> participanteList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery()) {
            while (result.next()) {
                Participante participante = new Participante();
                participante.setUid(result.getLong("uid"));
                participante.setLogin(result.getString("login"));
                participante.setNome_completo(result.getString("nome_completo"));
                participante.setTipo(result.getString("tipo"));
                
                participanteList.add(participante);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            throw new SQLException("Erro ao listar usuários.");
        }

        return participanteList;
    }

    public void authenticate(Participante participante) throws SQLException, SecurityException {
        
        
        try (PreparedStatement statement = connection.prepareStatement(authenticateQuery);) {
            statement.setString(1, participante.getLogin());
            statement.setString(2, participante.getSenha());

            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    participante.setUid(result.getLong("uid"));
                    participante.setNome_completo(result.getString("nome_completo"));
                    participante.setTipo(result.getString("tipo"));
                    participante.setFoto(result.getBinaryStream("foto"));
                    
                } else {
                    throw new SecurityException("Login ou senha incorretos.");
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao autenticar usuário.");
        }
    }    
    
    private static java.sql.Date getCurrentDate() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }

    @Override
    public void inscreve(long id, long id2,String parse) {

    try (PreparedStatement statement = connection.prepareStatement(inscreveQuery);) {
            statement.setLong(1, id);
            statement.setLong(2, id2);
            statement.setLong(3, 0);
            statement.setDate(4, getCurrentDate());
            statement.setLong(5, 0);
            statement.setString(6,parse);
            System.err.println(parse);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro na inscricao: evento não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro na inscricao: evento não encontrado.")) {
                try {
                    throw ex;
                } catch (SQLException ex1) {
                    Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    throw new SQLException("Erro ao se inscrever no evento.");
                } catch (SQLException ex1) {
                    Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }   
    }
    
       @Override
    public List<Participante> listaInscricoes(int parseInt, String parameter) throws SQLException{
        
        List<Participante> participanteList = new ArrayList<>();
        List<Inscricao> part = new ArrayList<>();
        PreparedStatement statement = null;
        PreparedStatement stat = null;
        int s = 0;


        try{
            if (null != parameter)switch (parameter) {
                case "data":
                    statement = connection.prepareStatement(readInscreveQuerydt);
                    break;
                case "nome":
                    statement = connection.prepareStatement(readInscreveQuery);
                    s = 1;
                    break;
                case "status":
                    statement = connection.prepareStatement(readInscreveQueryst);
                    break;
                case "nada":
                    statement = connection.prepareStatement(readInscreveQuery);
                    break;
                default:
                    break;
            }
            
            statement.setInt(1, parseInt);
          
            ResultSet rs = statement.executeQuery();
            
            while (rs.next()) {
                Inscricao inscricao = new Inscricao();
                inscricao.setId_participante(rs.getInt("id_participante"));
                inscricao.setData(rs.getDate("data_inscricao"));
                inscricao.setPago(rs.getInt("pago"));
                inscricao.setPresenca(rs.getInt("presenca"));
                inscricao.setForma_pagamento(rs.getString("forma_pagamento"));
                inscricao.setId_evento(parseInt);
                
                part.add(inscricao);

            }
            
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            throw new SQLException("Erro ao listar participantes.");
        }
        
        for (Iterator iterator = part.iterator(); iterator.hasNext(); ) {  
            Inscricao c = (Inscricao) iterator.next();  
                        
                try{

                    stat = connection.prepareStatement(searchP);
                    stat.setInt(1, c.getId_participante());
                     
                     ResultSet r = stat.executeQuery();
                     
                      while (r.next()) {
                          Participante participante = new Participante();
                          Inscricao inscricao = new Inscricao();
                          participante.setNome_completo(r.getString("nome_completo"));
                          participante.setUid(c.getId_participante());
                          inscricao.setData(c.getData());
                          inscricao.setId_evento(c.getId_evento());
                          inscricao.setPago(c.getPago());
                          inscricao.setPresenca(c.getPresenca());
                          participante.setInscricao(inscricao);
                          
                          participanteList.add(participante);
                      }
                }catch (SQLException e){
                    System.err.println(e.getMessage());
                    throw new SQLException("Erro ao listar participantes.");
                }
            
        }    
        if(s==1){
            Collections.sort(participanteList, Participante.COMPARE_BY_NAME);
        }

        return participanteList;
    }

    
    @Override
    public List<Participante> busca(Evento ev, int op) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancelaInsc(long parseLong,long parseLong0) {
        try (PreparedStatement statement = connection.prepareStatement(cancelaInsc);) {
            statement.setLong(1, parseLong);
            statement.setLong(2, parseLong0);
            

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao excluir: evento não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao excluir: usuário não encontrado.")) {
                try {
                    throw ex;
                } catch (SQLException ex1) {
                    Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    throw new SQLException("Erro ao excluir usuário.");
                } catch (SQLException ex1) {
                    Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }    
    }

    @Override
    public void pagaEvento(long parseLong, long parseLong0) {
        try (PreparedStatement statement = connection.prepareStatement(pagaInsc);) {
                statement.setLong(1, parseLong);
                statement.setLong(2, parseLong0);


                if (statement.executeUpdate() < 1) {
                    throw new SQLException("Erro ao pagar: evento não encontrado.");
                }
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());

                if (ex.getMessage().equals("Erro ao pagar: usuário não encontrado.")) {
                    try {
                        throw ex;
                    } catch (SQLException ex1) {
                        Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                } else {
                    try {
                        throw new SQLException("Erro ao pagar evento.");
                    } catch (SQLException ex1) {
                        Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }        
    }

    @Override
    public void presencaEvento(long parseLong, long parseLong0) {
          try (PreparedStatement statement = connection.prepareStatement(confirmaPres);) {
                statement.setLong(1, parseLong);
                statement.setLong(2, parseLong0);

                if (statement.executeUpdate() < 1) {
                    throw new SQLException("Erro ao confirmar presenca: evento não encontrado.");
                }
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());

                if (ex.getMessage().equals("Erro ao confirmar presenca: usuário não encontrado.")) {
                    try {
                        throw ex;
                    } catch (SQLException ex1) {
                        Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                } else {
                    try {
                        throw new SQLException("Erro ao confirmar presenca.");
                    } catch (SQLException ex1) {
                        Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
            }        
    }

    @Override
    public List<Participante> listaPresentes(int parseInt) {
            List<Participante> participanteList = new ArrayList<>();
            List<Inscricao> part = new ArrayList<>();
            PreparedStatement statement = null;
            PreparedStatement stat = null;


            try{
                statement = connection.prepareStatement(listaPresentes);
                statement.setInt(1, parseInt);

                ResultSet rs = statement.executeQuery();

                while (rs.next()) {
                    Inscricao inscricao = new Inscricao();
                    inscricao.setId_participante(rs.getInt("id_participante"));
                    inscricao.setData(rs.getDate("data_inscricao"));
                    inscricao.setPago(rs.getInt("pago"));
                    inscricao.setId_evento(parseInt);

                    part.add(inscricao);

                }

            }catch (SQLException e) {
                System.err.println(e.getMessage());
                try {
                    throw new SQLException("Erro ao listar participantes.");
                } catch (SQLException ex) {
                    Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for (Iterator iterator = part.iterator(); iterator.hasNext(); ) {  
                Inscricao c = (Inscricao) iterator.next();  

                    try{
                         stat = connection.prepareStatement(searchP);
                         stat.setInt(1, c.getId_participante());

                         ResultSet r = stat.executeQuery();

                          while (r.next()) {
                              Participante participante = new Participante();
                              Inscricao inscricao = new Inscricao();
                              participante.setNome_completo(r.getString("nome_completo"));
                              participante.setUid(c.getId_participante());
                              inscricao.setData(c.getData());
                              inscricao.setId_evento(c.getId_evento());
                              inscricao.setPago(c.getPago());
                              participante.setInscricao(inscricao);

                              participanteList.add(participante);
                          }
                    }catch (SQLException e){
                        System.err.println(e.getMessage());
                    try {
                        throw new SQLException("Erro ao listar participantes.");
                    } catch (SQLException ex) {
                        Logger.getLogger(ParticipanteDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    }

            }      
            return participanteList;    
    }

    @Override
    public Participante retorna(int id_entidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Integer> calcula(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estatistica estatisticas(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Participante> estatisticasm(String[] evs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void libera(int id,int op) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importaInsc(List<Participante> ev, int id4) {
        for(Participante p : ev){
            inscreve(p.getUid(), id4, p.getInscricao().getForma_pagamento());
        }    
    }




   
}
