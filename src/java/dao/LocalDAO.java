/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dao.EventoDAO.createQuery;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Estatistica;
import model.Evento;
import model.Local;
import model.Participante;

/**
 *
 * @author rykka
 */


public class LocalDAO extends DAO<Local>{

    static String createQuery = "INSERT INTO public.local(\n" +
"            nome_local, endereco_local, coord_geo,tipo_telefone,tel_telefone,ddd_telefone)\n" +
"    VALUES (?, ?, ?,?,?,?) RETURNING id_local";
    
    static String allQuery = "SELECT * FROM public.local";
    
    static String searchL = "SELECT * FROM public.local WHERE id_local=?";
    
    public LocalDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Local t) throws SQLException {
    try(PreparedStatement statement = connection.prepareStatement(createQuery)){
            statement.setString(1,t.getNome_local());
            statement.setString(2,t.getEndereco());
            statement.setString(3,t.getCoord_geo());
            statement.setString(4,t.getTipo_telefone());
            statement.setString(5, t.getTel_telefone());
            statement.setString(6, t.getDdd());
            
            
            try (ResultSet result = statement.executeQuery()) {
                if(result.next()){
                    t.setId_local(result.getInt("id_local"));
                }
            }
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uk_evento")) {
                throw new SQLException("Erro ao inserir local: local já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao inserir local: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao inserir local.");
            }
        }    
    }

  
    @Override
    public List<Local> all() throws SQLException {
    
        List<Local> localList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery()) {
            while (result.next()) {
               
                Local local = new Local();
                               
                local.setId_local(result.getInt("id_local"));
                local.setNome_local(result.getString("nome_local"));
                local.setEndereco(result.getString("endereco_local"));
                local.setCoord_geo(result.getString("coord_geo"));
                local.setDdd(result.getString("ddd_telefone"));
                local.setTel_telefone(result.getString("tel_telefone"));
                local.setTipo_telefone(result.getString("tipo_telefone"));
                
                localList.add(local); 
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar locais.");
        }

        return localList;    
    }
    
    @Override
    public Local retorna(int id_entidade) {
        Local local = new Local();
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement(searchL);
            statement.setInt(1, id_entidade);
          
            ResultSet rs = statement.executeQuery();
            
            if (rs.next()) {
              local.setId_local(id_entidade);
              local.setNome_local(rs.getString("nome_local"));
              local.setEndereco(rs.getString("endereco_local"));
              local.setCoord_geo(rs.getString("coord_geo"));
              local.setDdd(rs.getString("ddd_telefone"));
              local.setTel_telefone(rs.getString("tel_telefone"));
              local.setTipo_telefone(rs.getString("tipo_telefone"));
            }
            
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao buscar entidade.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return local;
    }

    @Override
    public void inscreve(long id, long id2,String parse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Local> busca(Evento ev, int op) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public void cancelaInsc(long parseLong,long parseLong0){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pagaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presencaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Local> listaPresentes(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
   @Override
    public Local read(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Local t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Integer> calcula(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Local> listaInscricoes(int parseInt, String parameter) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estatistica estatisticas(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Local> estatisticasm(String[] evs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void libera(int id,int op) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importaInsc(List<Participante> ev, int id4) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
