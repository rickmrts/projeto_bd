package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.EntidadePromotora;
import model.Estatistica;
import model.Evento;
import model.Inscricao;
import model.Inst_Orig;
import model.Local;
import model.Participante;
import model.Telefone;



/**
 *
 * @author rykka
 */
public class EventoDAO extends DAO<Evento> {
    
    static String createQuery = "INSERT INTO eventos(titulo, descricao, informacoes_imp, preco, dt_inicio, forma_pagamento, \n" +
"            id_local, id_entidade,dt_final,liberado)" + "VALUES ( ?, ?, ?, ?, ?, ?, ?, \n" +
"            ?,?,?) RETURNING uid";
    static String readQuery = "SELECT titulo, descricao, informacoes_imp,dt_inicio,dt_final, preco, id_local, id_entidade,liberado\n" +
"  FROM eventos WHERE uid = ?";
    static String deleteQuery = "DELETE FROM eventos WHERE uid=?";
    static String allQuery = "SELECT * FROM eventos;";
    static String updateQuery = "UPDATE eventos SET titulo=?, descricao=?, informacoes_imp=?,dt_inicio=?, preco=?, id_local=?, id_entidade=?,dt_final=?,liberado=? WHERE uid=?";
    static String searchQueryt = "SELECT * FROM eventos WHERE titulo=?";
    static String searchQuerye = "SELECT * FROM entidade WHERE nome_entidade=?";
    static String searchQueryp = "SELECT * FROM eventos WHERE dt_inicio::date>=date(?) AND dt_final::date<=date(?)";
    static String readInscreveQuery = "SELECT * FROM inscricoes WHERE id_participante=?";
    static String searchE = "SELECT * FROM eventos WHERE uid=?";
    static String searchQ = "SELECT * FROM eventos WHERE id_entidade=?";
    static String seleciona = "SELECT id_evento FROM inscricoes WHERE pago=1 AND id_evento=?";
    static String calculaRenda = "SELECT SUM(preco) AS valor FROM eventos WHERE uid=?";
    static String InscTotal = "SELECT COUNT(*) AS valor FROM inscricoes WHERE id_evento=?";
    static String InscSexo = "SELECT COUNT(q.uid) AS valor FROM (SELECT id_participante FROM public.inscricoes WHERE id_evento=?) AS p, public.participante AS q WHERE p.id_participante = q.uid AND q.sexo = ?";
    static String InscFxEt = "SELECT COUNT (P) AS valor FROM (SELECT extract(year from age(data_nasc)) AS P FROM public.participante AS part, (SELECT id_participante FROM public.inscricoes WHERE id_evento=?) AS insc WHERE part.uid=insc.id_participante ) AS Q WHERE Q.P>? AND Q.P<=?";
    static String InscFxEt_S = "SELECT COUNT (P) AS valor FROM (SELECT extract(year from age(data_nasc)) AS P,sexo FROM public.participante AS part, (SELECT id_participante FROM public.inscricoes WHERE id_evento=?) AS insc WHERE part.uid=insc.id_participante ) AS Q WHERE Q.P>? AND Q.P<=? AND Q.sexo=?";
    static String Insc_Inst = "SELECT COUNT(inst_orig) AS qtd,UPPER(inst_orig) AS nome FROM (SELECT id_participante FROM public.inscricoes WHERE id_evento = ?) AS P,public.participante AS Q WHERE P.id_participante=Q.uid GROUP BY UPPER(inst_orig)";
    static String liberaQuery = "UPDATE eventos SET liberado=? WHERE uid=?";
    
    public EventoDAO(Connection connection) {
        super(connection);
    }
   
    @Override
    public void create(Evento t) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery)){

            statement.setString(1,t.getTitulo());
            statement.setString(2,t.getDescricao());
            statement.setString(3,t.getInformacoes_imp());
            statement.setInt(4,t.getPreco());
            statement.setDate(5,t.getDt_inicio());
            statement.setString(6, t.getInformacoes_imp());
            statement.setInt(7, t.getLocal().getId_local());
            statement.setInt(8, t.getEntidade().getId_entidade());
            statement.setDate(9,t.getDt_final());
            statement.setInt(10, t.getLiberado());
            
            System.err.print(t);
            
            try (ResultSet result = statement.executeQuery()) {
                if(result.next()){
                    t.setUid(result.getLong("uid"));
                }
            }
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uk_evento")) {
                throw new SQLException("Erro ao inserir evento: evento já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao inserir evento: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao inserir evento.");
            }
        }
    }

    @Override
    public Evento read(Long id) throws SQLException {
        Evento evento = new Evento();
        EntidadePromotora entidade = new EntidadePromotora();
        Local local = new Local();
          
        try(PreparedStatement statement = connection.prepareStatement(readQuery)){
            statement.setLong(1,id);
            
            ResultSet result = statement.executeQuery();
            
            if(result.next()){
                evento.setUid(id);
                evento.setTitulo(result.getString("titulo"));
                evento.setDescricao(result.getString("descricao"));
                evento.setInformacoes_imp(result.getString("informacoes_imp"));
                local.setId_local(result.getInt("id_local"));
                entidade.setId_entidade(result.getInt("id_entidade"));
                
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    DAO dao = daoFactory.getLocalDAO();
                    DAO dao2 = daoFactory.getEntidadeDAO();
                    
                    Local loc = new Local();
                    EntidadePromotora ent = new EntidadePromotora();
                    
                    loc = (Local) dao.retorna(local.getId_local());
                    ent = (EntidadePromotora) dao2.retorna(entidade.getId_entidade());
                                     
                    evento.setLocal(loc);
                    evento.setEntidade(ent);

                } catch (ClassNotFoundException | IOException | SQLException ex) {
                   
                }
                 
           
                evento.setLocal(local);
                evento.setEntidade(entidade);
                evento.setDt_inicio(result.getDate("dt_inicio"));
                evento.setDt_final(result.getDate("dt_final"));
                evento.setPreco(result.getInt("preco"));
                evento.setLiberado(result.getInt("liberado"));
                
            } else {
                    throw new SQLException("Erro ao visualizar: evento não encontrado.");
                }
        }
        catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao visualizar: usuário não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao visualizar evento.");
            }
        }

        return evento;
    }

    @Override
    public void update(Evento t) throws SQLException {

        try (PreparedStatement statement = connection.prepareStatement(updateQuery)) {
            statement.setString(1,t.getTitulo());
            statement.setString(2,t.getDescricao());
            statement.setString(3,t.getInformacoes_imp());
            statement.setDate(4,t.getDt_inicio());
            statement.setInt(5,t.getPreco());
            statement.setInt(6, t.getLocal().getId_local());
            statement.setInt(7, t.getEntidade().getId_entidade());
            statement.setDate(8, t.getDt_final());
            statement.setLong(9, t.getUid());
            statement.setInt(10, t.getLiberado());

        if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao editar: evento não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao editar: evento não encontrado.")) {
                throw ex;
            } else if (ex.getMessage().contains("uk_evento")) {
                throw new SQLException("Erro ao editar evento: evento já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao editar evento: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao editar evento.");
            }
        }
          
    }

    @Override
    public void delete(Long id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setLong(1, id);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro ao excluir: evento não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro ao excluir: evento não encontrado.")) {
                throw ex;
            } else {
                throw new SQLException("Erro ao excluir evento.");
            }
        }
    }
 
    @Override
    public List<Evento> all() throws SQLException {
        
        List<Evento> eventoList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
                ResultSet result = statement.executeQuery()) {
            while (result.next()) {
                Evento evento = new Evento();
                EntidadePromotora entidade = new EntidadePromotora();
                Local local = new Local();
                
                evento.setUid(result.getInt("uid"));
                evento.setTitulo(result.getString("titulo"));
                evento.setDescricao(result.getString("descricao"));
                evento.setInformacoes_imp(result.getString("informacoes_imp"));
                local.setId_local(result.getInt("id_local"));
                entidade.setId_entidade(result.getInt("id_entidade"));
                
               
                try(DAOFactory daoFactory = new DAOFactory();){
                    DAO dao = daoFactory.getLocalDAO();
                    DAO dao2 = daoFactory.getEntidadeDAO();
                    
                    Local loc = new Local();
                    EntidadePromotora ent = new EntidadePromotora();
                    
                    loc = (Local) dao.retorna(local.getId_local());
                    ent = (EntidadePromotora) dao2.retorna(entidade.getId_entidade());
                                     
                    evento.setLocal(loc);
                    evento.setEntidade(ent);

                } catch (ClassNotFoundException | IOException | SQLException ex) {
                   
                }
                evento.setDt_inicio(result.getDate("dt_inicio"));
                evento.setDt_final(result.getDate("dt_final"));
                evento.setPreco(result.getInt("preco"));
                evento.setPagamento(result.getString("forma_pagamento"));
                evento.setLiberado(result.getInt("liberado"));
              
                eventoList.add(evento);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar eventos.");
        }

        return eventoList;

    }

    
    public List<Evento> busca(Evento ev,int op) throws SQLException{
       List<Evento> buscaList = new ArrayList<>();
       PreparedStatement preparedStatement = null;
           try {
            switch(op){
                case 1:
                    preparedStatement = connection.prepareStatement(searchQueryt);
                    preparedStatement.setString(1,ev.getTitulo());
                    break;
                case 2:
                    preparedStatement = connection.prepareStatement(searchQuerye);
                    preparedStatement.setString(1,ev.getEntidade().getNome_entidade());
                    break;
                case 3:
                    preparedStatement = connection.prepareStatement(searchQueryp);
                    preparedStatement.setDate(1,ev.getDt_inicio());
                    preparedStatement.setDate(2,ev.getDt_final());
                    break;
            }
            
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Evento evento = new Evento();
                EntidadePromotora entidade = new EntidadePromotora();
                List <Evento> lista = new ArrayList<>();
                
                switch(op){
                
                    case 1:
                        evento.setTitulo(rs.getString("titulo"));
                        evento.setPreco(rs.getInt("preco"));
                        evento.setDt_inicio(rs.getDate("dt_inicio"));
                        evento.setDt_final(rs.getDate("dt_final"));
                        entidade.setId_entidade(rs.getInt("id_entidade"));
                         try(DAOFactory daoFactory = new DAOFactory();){
                               DAO dao = daoFactory.getEntidadeDAO();
                               EntidadePromotora ent = new EntidadePromotora();
                               ent = (EntidadePromotora) dao.retorna(entidade.getId_entidade());
                               evento.setEntidade(ent);
                               } catch (ClassNotFoundException | IOException | SQLException ex) {}
                    break;
                        
                    case 2:
                        entidade.setId_entidade(rs.getInt("id_entidade")); 
                        evento.setEntidade(entidade);
                        try(DAOFactory daoFactory = new DAOFactory();){
                               DAO dao = daoFactory.getEventoDAO();
                               evento = (Evento) dao.retorna(rs.getInt("id_entidade"));
                               } catch (ClassNotFoundException | IOException | SQLException ex) {}
                        break;
                    
                    case 3:
                        evento.setTitulo(rs.getString("titulo"));
                        evento.setPreco(rs.getInt("preco"));
                        evento.setDt_inicio(rs.getDate("dt_inicio"));
                        evento.setDt_final(rs.getDate("dt_final"));
                        entidade.setId_entidade(rs.getInt("id_entidade"));
                         try(DAOFactory daoFactory = new DAOFactory();){
                               DAO dao = daoFactory.getEntidadeDAO();
                               EntidadePromotora ent = new EntidadePromotora();
                               ent = (EntidadePromotora) dao.retorna(entidade.getId_entidade());
                               evento.setEntidade(ent);
                               } catch (ClassNotFoundException | IOException | SQLException ex) {}
                         
                    break;
                }
                   
                    buscaList.add(evento);
            }
        } catch (SQLException e) {e.printStackTrace();}
        return buscaList;
    }

    @Override
    public List<Evento> listaInscricoes(int parseInt,String parameter) throws SQLException {
       
        List<Evento> eventoList = new ArrayList<>();
        List<Inscricao> part = new ArrayList<>();
        PreparedStatement statement = null;
        PreparedStatement stat = null;


        try{
            statement = connection.prepareStatement(readInscreveQuery);
            statement.setInt(1, parseInt);
          
            ResultSet rs = statement.executeQuery();
            
            while (rs.next()) {
                Inscricao inscricao = new Inscricao();
                inscricao.setId_participante(parseInt);
                inscricao.setData(rs.getDate("data_inscricao"));
                inscricao.setPago(rs.getInt("pago"));
                inscricao.setId_evento(rs.getInt("id_evento"));
                
                part.add(inscricao);

            }
            
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            throw new SQLException("Erro ao listar participantes.");
        }
        
        for (Iterator iterator = part.iterator(); iterator.hasNext(); ) {  
            Inscricao c = (Inscricao) iterator.next();  
                        
                try{
                     stat = connection.prepareStatement(searchE);
                     stat.setInt(1, c.getId_evento());
                     
                     ResultSet r = stat.executeQuery();
                     
                      while (r.next()) {
                          Evento evento = new Evento();
                          evento.setUid(c.getId_evento());
                          evento.setPreco(c.getId_participante());
                          evento.setTitulo(r.getString("titulo"));
                          eventoList.add(evento);
                      }
                }catch (SQLException e){
                    System.err.println(e.getMessage());
                    throw new SQLException("Erro ao listar eventos.");
                }
            
        }      
        return eventoList;    
    }
        
    private static java.sql.Date getCurrentDate() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }
    @Override
    public void inscreve(long id,long id2,String parse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancelaInsc(long parseLong,long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pagaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presencaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Evento> listaPresentes(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Evento retorna(int id_entidade) {
        Evento evento = new Evento();
        EntidadePromotora entidade = new EntidadePromotora();
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement(searchQ);
            statement.setInt(1, id_entidade);
          
            ResultSet rs = statement.executeQuery();
            
            if (rs.next()) {
                entidade.setId_entidade(id_entidade);
                evento.setEntidade(entidade);
                evento.setTitulo(rs.getString("titulo"));
                evento.setPreco(rs.getInt("preco"));
                evento.setDt_inicio(rs.getDate("dt_inicio"));
                evento.setDt_final(rs.getDate("dt_final"));
            }
            
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao buscar entidade.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return evento;   
    }

    @Override
    public List<Integer> calcula(int parseInt) {
        Evento evento = new Evento();
        List<Inscricao> inscr = new ArrayList<>();
        List<Integer> valor=new ArrayList<>();
        
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement(seleciona);
            statement.setInt(1,parseInt);
                    
            ResultSet rs = statement.executeQuery();
            
            while (rs.next()) {
                Inscricao inscricao = new Inscricao();
                inscricao.setId_evento(rs.getInt("id_evento"));
                inscr.add(inscricao);
            }
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao buscar entidade.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        PreparedStatement stat = null;
        
       for (Iterator iterator = inscr.iterator(); iterator.hasNext(); ) {  
            Inscricao c = (Inscricao) iterator.next();  
                        
                try{
                     stat = connection.prepareStatement(calculaRenda);
                     stat.setInt(1, c.getId_evento());
                     
                     ResultSet r = stat.executeQuery();
                     
                     while (r.next()){
                        valor.add(r.getInt("valor"));
                     }
                }catch (SQLException e){
                    System.err.println(e.getMessage());
                try {
                    throw new SQLException("Erro ao listar inscricoes.");
                } catch (SQLException ex) {
                    Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }      
        return valor;      
    }

    @Override
    public Estatistica estatisticas(int parseInt) {
         PreparedStatement statement = null;
         Estatistica valor = new Estatistica();
         int v[] = new int[3];
         int w[] = new int[7];
         int x1[] = new int[7];
         int x2[] = new int[7];
         List<Inst_Orig> x = new ArrayList<>();
         
         try{
            statement = connection.prepareStatement(InscTotal);
            statement.setInt(1,parseInt);
            
             ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   valor.setTotal(rs.getInt("valor"));
             }
         }
         catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        statement = null;
        
        try{
            statement = connection.prepareStatement(InscSexo);
            statement.setInt(1,parseInt);
            statement.setString(2, "masculino");
            
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   v[0] = rs.getInt("valor");
             }
          
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        statement = null;
        try{
            statement = connection.prepareStatement(InscSexo);
            statement.setInt(1,parseInt);
            statement.setString(2, "feminino");
            
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   v[1] = rs.getInt("valor");
             }
          
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        statement = null;
        try{
            statement = connection.prepareStatement(InscSexo);
            statement.setInt(1,parseInt);
            statement.setString(2, "outro");
            
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   v[2] = rs.getInt("valor");
             }
          
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        valor.setPsexo(v);
        
        statement = null;
        try{
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,0);
            statement.setInt(3,10);
            
     
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   w[0] = rs.getInt("valor"); //faixa etaria >0 e <=10
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,10);
            statement.setInt(3,20);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[1] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,20);
            statement.setInt(3,30);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[2] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,30);
            statement.setInt(3,40);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[3] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,40);
            statement.setInt(3,50);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[4] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
          
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,50);
            statement.setInt(3,60);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[5] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt);
            statement.setInt(1,parseInt);
            statement.setInt(2,60);
            statement.setInt(3,100);
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   w[6] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        valor.setFx_et(w);
        
        
        statement = null;
        try{
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,0);
            statement.setInt(3,10);
            statement.setString(4, "masculino");
            
     
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   x1[0] = rs.getInt("valor"); //faixa etaria >0 e <=10
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,10);
            statement.setInt(3,20);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[1] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,20);
            statement.setInt(3,30);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[2] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,30);
            statement.setInt(3,40);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[3] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,40);
            statement.setInt(3,50);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[4] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
          
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,50);
            statement.setInt(3,60);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[5] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,60);
            statement.setInt(3,100);
            statement.setString(4, "masculino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x1[6] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        valor.setSex_fx_et_m(x1);
        statement = null;
        
        try{
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,0);
            statement.setInt(3,10);
            statement.setString(4, "feminino");
            
     
            ResultSet rs = statement.executeQuery();
             while(rs.next()){
                   x2[0] = rs.getInt("valor"); //faixa etaria >0 e <=10
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,10);
            statement.setInt(3,20);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[1] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,20);
            statement.setInt(3,30);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[2] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,30);
            statement.setInt(3,40);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[3] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
             
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,40);
            statement.setInt(3,50);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[4] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
          
             statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,50);
            statement.setInt(3,60);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[5] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
            statement = null;
             
            statement = connection.prepareStatement(InscFxEt_S);
            statement.setInt(1,parseInt);
            statement.setInt(2,60);
            statement.setInt(3,100);
            statement.setString(4, "feminino");
            
     
            rs = statement.executeQuery();
             while(rs.next()){
                   x2[6] = rs.getInt("valor"); //faixa etaria >10 e <=20
             }
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        valor.setSex_fx_et_f(x2);
        
        statement = null;
        try{
            statement = connection.prepareStatement(Insc_Inst);
            statement.setInt(1,parseInt);

            
        ResultSet rs = statement.executeQuery();
             while(rs.next()){
                 Inst_Orig aux = new Inst_Orig();
                 
                 aux.setQtd(rs.getInt("qtd"));
                 aux.setInst(rs.getString("nome"));
                 
                 x.add(aux);
                   
             }
          
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao calcular estatistíca.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        valor.setInst(x);
        
        
        return valor;
    }

    @Override
    public List<Evento> estatisticasm(String[] evs) {
        List<Evento> eventos = new ArrayList<Evento>();
        List<Long> x = new ArrayList<>();
        
        for(String s : evs){
            x.add(Long.valueOf(s));
        }

        
        for( int i = 0; i < x.size(); i++)
        {
            try {
                Evento ev = new Evento();
                ev = read(x.get(i));
                eventos.add(ev);
            } catch (SQLException ex) {
                Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       return eventos;
    }  

    @Override
    public void libera(int id,int op) {
     try (PreparedStatement statement = connection.prepareStatement(liberaQuery);) {
            statement.setLong(1, op);   
            statement.setLong(2, id);
           
            if (statement.executeUpdate() < 1) {
                throw new SQLException("Erro na liberacao: evento não encontrado.");
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().equals("Erro na liberacao: evento não encontrado.")) {
                try {
                    throw ex;
                } catch (SQLException ex1) {
                    Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } else {
                try {
                    throw new SQLException("Erro ao liberar o evento.");
                } catch (SQLException ex1) {
                    Logger.getLogger(EventoDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }      
    
    }

    @Override
    public void importaInsc(List <Participante> ev, int id4) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    

    
}
   
 

