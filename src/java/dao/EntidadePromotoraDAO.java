/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.EntidadePromotora;
import model.Estatistica;
import model.Evento;
import model.Participante;


/**
 *
 * @author rykka
 */
public class EntidadePromotoraDAO extends DAO <EntidadePromotora>{

    static String createQuery = "INSERT INTO public.entidade(\n" +
"            nome_entidade, descricao_entidade)\n" +
"    VALUES (?, ?) RETURNING id_entidade";
    
     static String allQuery = "SELECT * FROM public.entidade";
     
     static String searchE = "SELECT * FROM public.entidade WHERE id_entidade=?";
    
    
    public EntidadePromotoraDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(EntidadePromotora t) throws SQLException {
     try(PreparedStatement statement = connection.prepareStatement(createQuery)){
            statement.setString(1,t.getNome_entidade());
            statement.setString(2,t.getDescricao_entidade());
           
            
            try (ResultSet result = statement.executeQuery()) {
                if(result.next()){
                    t.setId_entidade(result.getInt("id_entidade"));
                }
            }
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());

            if (ex.getMessage().contains("uk_evento")) {
                throw new SQLException("Erro ao inserir entidade: entidade já existente.");
            } else if (ex.getMessage().contains("not-null")) {
                throw new SQLException("Erro ao inserir entidade: pelo menos um campo está em branco.");
            } else {
                throw new SQLException("Erro ao inserir entidade.");
            }
        }       
    }

  

    @Override
    public List<EntidadePromotora> all() throws SQLException {
        List<EntidadePromotora> entidadeList = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(allQuery);
            ResultSet result = statement.executeQuery()) {
            while (result.next()) {
               
                EntidadePromotora entidade = new EntidadePromotora();
                
                
                entidade.setId_entidade(result.getInt("id_entidade"));
                entidade.setNome_entidade(result.getString("nome_entidade"));
                entidade.setDescricao_entidade(result.getString("descricao_entidade"));
                
                entidadeList.add(entidade);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

            throw new SQLException("Erro ao listar entidades.");
        }

        return entidadeList;        
    }

   
    
    @Override
    public EntidadePromotora retorna(int id_entidade) {
        
        EntidadePromotora entidade = new EntidadePromotora();
        PreparedStatement statement = null;
        
        try{
            statement = connection.prepareStatement(searchE);
            statement.setInt(1, id_entidade);
          
            ResultSet rs = statement.executeQuery();
            
            if (rs.next()) {
              entidade.setId_entidade(id_entidade);
              entidade.setNome_entidade(rs.getString("nome_entidade"));
              entidade.setDescricao_entidade(rs.getString("descricao_entidade"));
            }
            
        }catch (SQLException e) {
            System.err.println(e.getMessage());
            try {
                throw new SQLException("Erro ao buscar entidade.");
            } catch (SQLException ex) {
                Logger.getLogger(EntidadePromotoraDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return entidade;
    }

      @Override
    public EntidadePromotora read(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(EntidadePromotora t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<EntidadePromotora> busca(Evento ev, int op) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public void cancelaInsc(long parseLong,long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pagaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presencaEvento(long parseLong, long parseLong0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadePromotora> listaPresentes(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void inscreve(long id, long id2, String parse) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Integer> calcula(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadePromotora> listaInscricoes(int parseInt, String parameter) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estatistica estatisticas(int parseInt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadePromotora> estatisticasm(String[] evs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void libera(int id,int op) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importaInsc(List<Participante> ev, int id4) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



 

}
