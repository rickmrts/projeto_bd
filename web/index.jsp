<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Sistema de Gerenciamento de Eventos</title>
    
    </head>
    <body class="w3-light-grey">
        
        
    <script>
   function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

 FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
    
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '237705679973800',
      xfbml      : true,
      version    : 'v2.6'
    });
  };
 
  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=name,id,email', function(response) {
      console.log('Successful login for: ' + response.name);
      
    });
  }

</script>


        <div class="w3-content" style="max-width:1400px">
            <header class="w3-container w3-center w3-padding-32"> 
            <h1><b>Sistema de Gerenciamento de Eventos</b></h1>
          
        </header>

           
        </div>
        
        <hr>
         <div class="w3-row">                   
        <div class="w3-content" style="max-width:1200px">
            
            <form class="form-signin" action="${pageContext.servletContext.contextPath}/login" method="POST">
                <h2 class="form-signin-heading">Login<h2>

                <label class="h4">Usuário</label><input class="form-control" type="text" name="login" required autofocus>
                <label class="h4">Senha</label><input class="form-control" type="password" name="senha" required>

                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            </form>
                <hr>
            <button><a href="${pageContext.servletContext.contextPath}/participante/create">
                Cadastrar novo usuário
                </a>   </button>
            <button><a href="${pageContext.servletContext.contextPath}/evento?acao=n">
                Eventos disponíveis
                </a>   </button>


        </div>
         </div>
</html>
