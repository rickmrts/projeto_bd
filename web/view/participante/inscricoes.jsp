<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="model.Participante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Inscricoes</title>

    </head>
    <body class="w3-light-grey">

        <div class="w3-content" style="max-width:1200px">
             <h1>Inscrições</h1>
            <table class="w3-table-all" >
                <thead>
                    <th>Titulo</th>
                    <th>Ação</th>
                    </thead>
                <tbody>
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                     <td>
                         <button><a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/cancelainsc?id=${u.uid}&&id2=${u.preco}" >
                                    Cancelar Inscrição
                             </a></button>
                                    
                     </td>
                </c:forEach>
                </tbody>
                
            </table>
            
            <br><br>
            <hr>
            <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
      </div>
    </body>
</html>
