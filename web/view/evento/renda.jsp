
<%@include file="/view/include/loginCheck.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Valor Total</title>
    </head>
    <body class="w3-light-grey">

        <div class="w3-content" style="max-width:1200px">
          <h3>Valor Arrecadado</h3>
                R$ <c:out value="${renda}"/>
            
                <hr>
            <button><a href="${pageContext.servletContext.contextPath}/evento/list">Voltar</a></button>
            <button> <a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
      </div>
    </body>
</html>
