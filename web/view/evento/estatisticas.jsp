<%@include file="/view/include/loginCheck.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Estatistica"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Estatisticas</title>
    </head>
    <body class="w3-light-grey">
         <div class="w3-content" style="max-width:1200px">
             <h2>Estatisticas</h2>
             <button><a href="${pageContext.servletContext.contextPath}/evento/estatisticasm">Estatisticas Múltiplas</a></button>
            <table  class="w3-table-all" >
                <thead>
                    
                    </thead>
                    <tbody>
                    
                    <td>Total de Inscritos: <c:out value="${estatisticas.total}"/> </td>
                    <td>Total de Inscritos por Sexo:<br> Masculino: <c:out value="${estatisticas.psexo[0]}"/><br> Feminino: <c:out value="${estatisticas.psexo[1]}"/><br> Outro: <c:out value="${estatisticas.psexo[2]}"/></td>
                    <td>Total de Inscritos por Faixa Etaria: <br>0-10 anos: <c:out value="${estatisticas.fx_et[0]}"/><br>11-20 anos: <c:out value="${estatisticas.fx_et[1]}"/><br>21-30 anos: <c:out value="${estatisticas.fx_et[2]}"/><br>31-40 anos: <c:out value="${estatisticas.fx_et[3]}"/><br>41-50 anos: <c:out value="${estatisticas.fx_et[4]}"/> <br>51-60 anos: <c:out value="${estatisticas.fx_et[5]}"/> <br>Maior que 60 anos: <c:out value="${estatisticas.fx_et[6]}"/>  </td>
                </tbody>
                
                <tbody>
                    <td>Total de Inscritos por Faixa Etaria e Sexo: <br>Masculino:<br>0-10 anos: <c:out value="${estatisticas.sex_fx_et_m[0]}"/><br>11-20 anos: <c:out value="${estatisticas.sex_fx_et_m[1]}"/><br>21-30 anos: <c:out value="${estatisticas.sex_fx_et_m[2]}"/><br>31-40 anos: <c:out value="${estatisticas.sex_fx_et_m[3]}"/><br>41-50 anos: <c:out value="${estatisticas.sex_fx_et_m[4]}"/> <br>51-60 anos: <c:out value="${estatisticas.sex_fx_et_m[5]}"/> <br>Maior que 60 anos: <c:out value="${estatisticas.sex_fx_et_m[6]}"/>  </td>
                    <td>Total de Inscritos por Faixa Etaria e Sexo: <br>Feminino: <br>0-10 anos: <c:out value="${estatisticas.sex_fx_et_f[0]}"/><br>11-20 anos: <c:out value="${estatisticas.sex_fx_et_f[1]}"/><br>21-30 anos: <c:out value="${estatisticas.sex_fx_et_f[2]}"/><br>31-40 anos: <c:out value="${estatisticas.sex_fx_et_f[3]}"/><br>41-50 anos: <c:out value="${estatisticas.sex_fx_et_f[4]}"/> <br>51-60 anos: <c:out value="${estatisticas.sex_fx_et_f[5]}"/> <br>Maior que 60 anos: <c:out value="${estatisticas.sex_fx_et_f[6]}"/>  </td>
                    <td>Total Inscritos por Instituição de origem: <br> <c:forEach var="u" items="${estatisticas.inst}"> <c:out value="${u.inst}"/>:<c:out value="${u.qtd}"/><br>  </c:forEach>
                    
                </tbody>
            </table>
                    <hr>
                    
           <button><a href="${pageContext.servletContext.contextPath}/evento">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
    </body>
</html>
