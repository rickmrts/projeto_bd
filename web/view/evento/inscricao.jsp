<%-- 
    Document   : inscricao
    Created on : Jan 18, 2017, 9:09:54 AM
    Author     : rykka
--%>
<%@include file="/view/include/loginCheck.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Inscricao</title>
    </head>
    <body class="w3-light-grey">
        <div class="w3-content" style="max-width:1200px">
         <form class="form-group" name="formPagamento" method="POST"  action="${pageContext.servletContext.contextPath}/evento/inscricao">                    
                               
                           <select class="form-control" name="forma_pagamento">
                               <option value = "dinheiro">Dinheiro</option>
                               <option value = "cartao de credito">Cartão de Crédito</option>
                               <option value = "cartao de debito">Cartão de Débito</option>
                               <option value = "cheque">Cheque</option>
                           </select>
             
                        <button class="btn btn-lg btn-primary" type="submit">Inscrever-se</button>
                             </form>
        </div>
    </body>
</html>
