<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Busca Eventos</title>
    </head>
    <body  class="w3-light-grey">

        <form method="POST" name="form" action="${pageContext.servletContext.contextPath}/evento/busca">
        <div class="w3-content" style="max-width:1200px">
            <br>
            <label class="h4">Buscar</label>
            <input class="form-control" type="text" name="palavra" value="${palavra}" >
            
            <br><br>
            <label class ="h4">Tipo de busca</label>
            <br>    
            <input type="radio" name="radios" VALUE="titulo" CHECKED>Título<br>
            <input type="radio" name="radios" VALUE="entidade_promotora">Entidade Promotora<br>
            <input type="radio" name="radios" VALUE="periodo">Período<br>
            <input type="date" name ="dt_inicio">
            <input type="date" name ="dt_final">
            
            
            <br><br>
            <button class="btn btn-lg btn-primary" type="submit">Buscar</button>
            
            <button><a href="${pageContext.servletContext.contextPath}/evento">Voltar</a></button>
      
        </div>
        </form>

    </body>
</html>
