<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page import="model.Participante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Resultado da busca</title>
    </head>
    <body  class="w3-light-grey">

        <div class="w3-content" style="max-width:1200px">
            
            <table class = "w3-table-all">
                <thead>
                    <th>Título</th>
                    <th>Entidade Promotora</th>
                    <th>Data Inicio</th>
                    <th>Data Fim</th>
                    <th>Preço</th>

                </thead>
                <tbody>
                    
                <c:forEach var="u" items="${eventoList}">
                    <tr>
                        <td><c:out value="${u.titulo}"/></td>
                        
                        <td><c:out value="${u.entidade.nome_entidade}"/></td>
                        <td><c:out value="${u.dt_inicio}"/></td>
                        <td><c:out value="${u.dt_final}"/></td>
                        <td><c:out value="${u.preco}"/></td>
                        </tr>                    
                </c:forEach>
                    
                </tbody>
            </table>
            
            <br><br>
            <hr>
            
            <button><a href="${pageContext.servletContext.contextPath}/evento/busca">Nova Busca</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/evento">Voltar</a></button>
            <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
     </div>

    </body>
</html>
