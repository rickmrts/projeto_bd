<%-- 
    Document   : create
    Created on : Nov 16, 2016, 6:45:11 PM
    Author     : rykka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
       <title>Cadastro de Entidade Promotora</title>
    </head>
    <body class="w3-light-grey">

        <div class="w3-content" style="max-width:1200px">
            
            <h2>Novo Cadastro de Entidade Promotora</h2>

            <c:choose>
                <c:when test="${param.acao == 'c'}">
                <form class="form-group" name="formEntidade" method="POST" action="${pageContext.servletContext.contextPath}/entidade/create">                    
                </c:when>
               
            </c:choose>

                    <label class="h4">Nome da Entidade</label>
                    <input class="form-control" type="text" name="nome_entidade" value="${u.nome}" required >

                    <label class="h4">Descricao da Entidade</label>
                    <input class="form-control" type="text" name="descricao_entidade" value="${u.descricao}" required >
                                    
                    <hr><button class="btn btn-lg btn-primary" type="submit">Cadastrar</button>
                    <button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
                    
            </form>

        </div>
    </body>
</html>
