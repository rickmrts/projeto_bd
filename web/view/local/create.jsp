<%-- 
    Document   : create.jsp
    Created on : Nov 16, 2016, 2:44:56 PM
    Author     : rykka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
       <title>Cadastro de Local</title>
    </head>

    <body class="w3-light-grey">
        <div class="w3-content" style="max-width:1200px">
            
            <h2>Novo Local</h2>

            <form class="form-group" name="formLocal" method="POST" action="${pageContext.servletContext.contextPath}/local/create">                    
              
                    <label class="h4">Nome do Local</label>
                    <input class="form-control" type="text" name="nome_local" value="${u.nome_local}" required >
                    <hr>
                    <label class="h4">Endereço do Local</label>
                    <input class="form-control" type="text" name="endereco_local" value="${u.endereco_local}" required >
                    <hr>          
                    <label class="h4">Coordenadas Geográficas</label>
                    <input class="form-control" type="text" name="coord_geo" value="${u.coord_geo}" >
                    <hr>
                    <label class="h4">Tipo de Telefone</label>
                   
                    <select name="tipo_telefone">
                      <option value = "Residencial">Residencial</option>
                      <option value = "Comercial">Comercial</option>
                      <option value = "Celular">Celular</option>
                      <option value = "Recado">Recado</option>
                    </select>
                    <hr>
                    <label class="h4">DDD do Telefone</label>
                    <input class="form-control" type="text" name="ddd_telefone" value="${u.ddd}" >
                    <hr>
                    <label class="h4">Telefone</label>
                    <input class="form-control" type="text" name="tel_telefone" value="${u.tel_telefone}" >

                    <hr>
                    <button class="btn btn-lg btn-primary" type="submit">Cadastrar</button>
                    
            </form>
<br><button><a href="${pageContext.servletContext.contextPath}/">Voltar</a></button>
                    <hr>
        </div>
    </body>
</html>
