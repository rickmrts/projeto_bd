<%@include file="/view/include/loginCheck.jsp"%>

<%@page import="java.util.List"%>
<%@page import="model.Evento"%>
<%@page import="model.Participante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <title>Inscricoes</title>

    </head>
    <body class="w3-light-grey">

        <div class="w3-content" style="max-width:1200px">
            <h2> Inscrições</h2>
            <table class ="w3-table-all"  >
                <thead>
                    <th>Nome</th>
                    <th>Data Inscrição</th>
                    <th>Status</th>
                </thead>
                <tbody>
                <c:forEach var="u" items="${participantesList}">
                    <c:set var="evento" scope="session" value="${u.inscricao.id_evento}"/>
                    <tr>
                        <td><c:out value="${u.nome_completo}"/></td>
                        <td><c:out value="${u.inscricao.data}"/></td>
                        
                        <c:choose>
                            <c:when test="${u.inscricao.pago==0}"><td> Não Pago</td></c:when>
                             <c:otherwise><td>Pago</td></c:otherwise>
                        </c:choose>
                             <td> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/cancelainsc?id=${u.inscricao.id_evento}&&id2=${u.uid}" >
                                    Cancelar Inscrição
                                 </a></td>
                                 <c:choose>
                                 <c:when test="${u.inscricao.pago==0}">
                                  <td> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/pagamento?id=${u.inscricao.id_evento}&&id2=${u.uid}" >
                                    Confirmar Pagamento
                                 </a></td>
                                 </c:when>
                                
                                 </c:choose>
    
                                 <c:choose>
                                 <c:when test="${u.inscricao.presenca==0}">
                                  <td> <a class="btn btn-default" href="${pageContext.servletContext.contextPath}/participante/presenca?id=${u.inscricao.id_evento}&&id2=${u.uid}" >
                                    Confirmar Presença
                                 </a></td>    
                                 </c:when>
                                
                                 </c:choose>
                             
                         </tr>     
                </c:forEach>
                </tbody>
                <tfoot></tfoot>
            </table>
               
            
            <br><br>
            <hr>

           
            <button><a href="${pageContext.servletContext.contextPath}/evento/list">Voltar</a></button>
             <button><a href="${pageContext.servletContext.contextPath}/logout">Logout</a></button>
             <button><a class="btn btn-default" href = "${pageContext.servletContext.contextPath}/evento/inscricoes?id=${evento}&&op=${"data"}">Ordena por data</a></button>
            <button> <a class="btn btn-default" href = "${pageContext.servletContext.contextPath}/evento/inscricoes?id=${evento}&&op=${"nome"}">Ordena por nome</a></button>
            <button> <a class="btn btn-default" href = "${pageContext.servletContext.contextPath}/evento/inscricoes?id=${evento}&&op=${"status"}">Ordena por status</a></button>
            
      </div>
    </body>
</html>
